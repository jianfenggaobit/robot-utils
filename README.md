# robot-utils
robot utils include utility functions for robotic control and computer vision development in Python.

## Installation
This package is tested with `Python >=3.10` and `PyTorch 2.0.1`

If you have conda installed, create a conda virtual environment with name `venv`. 
```
# e.g. with Python 3.10
conda create -n venv python=3.10
```
You can also use other virtual environments as you like.

Then clone this package and install in editable mode as follows.
```
git clone https://gitlab.com/jianfenggaobit/robot-utils.git
# or for H2T users
git clone git@git.h2t.iar.kit.edu:sw/machine-learning-control/robot-utils.git

cd robot-utils
pip install -e .
```