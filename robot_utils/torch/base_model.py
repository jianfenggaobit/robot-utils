import torch
import tensorboard_logger

from pathlib import Path
from abc import ABC, abstractmethod
from typing import Optional, Union, Dict, Callable, Type, List

from rich.table import Table
from rich.console import Group
from rich.progress import Progress, TextColumn
from rich.live import Live

from robot_utils import console
from torch.utils.data import DataLoader, BatchSampler, SequentialSampler, SubsetRandomSampler
from robot_utils.py.interact import user_warning
from robot_utils.py.filesystem import get_validate_path
from robot_utils.torch.torch_utils import get_device, split_indices
from robot_utils.serialize.dataclass import (dataclass, load_dataclass, save_to_yaml,
                                             dump_data, dump_data_as_dict, load_dict_from_yaml)


def get_table(title, dict_data: dict, table_list: list):
    table = Table(title=title)
    table_list.append(table)
    table.add_column("Key", justify="center", style="cyan")
    table.add_column("Value", justify="left", style="green")
    for k, v in dict_data.items():
        if isinstance(v, dict):
            table.add_row(k, f"see Sub-table: {k}")
            get_table(f"Sub-table: {k}", v, table_list)
        else:
            table.add_row(k, f"{v}")


def print_dataclass_as_table(data, title: str = None):
    if title is None:
        title = f"[bold green]Configuration [cyan]{type(data)}"
    table_list = []
    get_table(title, dump_data_as_dict(data), table_list)
    for t in table_list:
        console.log(t)


@dataclass
class ModelConfig:
    use_gpu:                bool = True
    in_feats:               Optional[int] = None
    out_feats:              Optional[int] = None
    re_init:                Optional[bool] = True
    init_scheme:            str = 'uniform_in_dim'
    require_input_grad:     bool = False

    @property
    def name(self):
        return self.__class__.__name__


class BaseModel(torch.nn.Module, ABC):
    def __init__(self, config: Union[str, Path, Dict, None] = None, model_path: Union[str, Path] = None):
        super(BaseModel, self).__init__()

        if model_path is None:
            raise ValueError('model_path cannot be None')
        self.model_path = get_validate_path(model_path, create=True)
        self.params_file = self.model_path / f"{self.name}_model_params.pt"
        self.params_file_best = self.model_path / f"{self.name}_model_params_best.pt"

        self.__load_config(config)
        self._build_model()

    def __load_config(self, config: Union[str, Path, Dict, None]):
        self.config_file = self.model_path / f"{self.name}_model_config.yaml"
        save_flag = True
        if config is None:
            if not self.config_file.is_file():
                console.log(f"[bold red]Either provide a cfg or ensure a cfg file exists in {self.model_path}")
                exit(1)
            config = self.config_file
            save_flag = False

        self._load_config(config)

        self.device = get_device(self.c.use_gpu)
        if save_flag:
            self.save_config()
            self.save_model_type()
        self.print(param=False)

    @property
    def name(self):
        return self.__class__.__name__

    def save_model_type(self):
        save_to_yaml(dict(model_type=self.name), self.model_path / "meta_info.yaml")

    @classmethod
    def get_model_type(cls, model_path: Path):
        from robot_utils.serialize.dataclass import load_dict_from_yaml
        d = load_dict_from_yaml(model_path / "meta_info.yaml")
        return d["model_type"]

    @abstractmethod
    def _load_config(self, config: Union[str, Path, Dict, None]) -> None:
        self.c = load_dataclass(ModelConfig, config)

    def save_config(self):
        dump_data(self.c, self.config_file)

    @abstractmethod
    def _build_model(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def forward(self, x: Union[torch.Tensor, Dict[str, torch.Tensor]], **kwargs):
        raise NotImplementedError

    def save_model(self, best_param: bool = False, optimizer=None, scheduler=None, **kwargs):
        filename = self.params_file_best if best_param else self.params_file
        dic = {"model": self.state_dict()}
        dic.update(kwargs)
        if optimizer is not None:
            dic["optimizer"] = optimizer.state_dict()
        if scheduler is not None:
            dic["scheduler"] = scheduler.state_dict()
        torch.save(dic, filename.as_posix())

    def load_model(self, optimizer=None, scheduler=None) -> dict:
        checkpoint = torch.load(self.params_file)  # type: dict
        self.load_state_dict(checkpoint["model"])
        checkpoint.pop("model", None)
        if optimizer is not None:
            optimizer.load_state_dict(checkpoint["optimizer"])
            checkpoint.pop("optimizer", None)
        if scheduler is not None and 'scheduler' in checkpoint:
            scheduler.load_state_dict(checkpoint["scheduler"])
            checkpoint.pop("scheduler", None)
        return checkpoint

    def print(self, param=True, info=True, shape_only=False, *args, **kwargs):
        if param:
            console.rule(f"[bold blue]Printing Parameters of {self.name}")
            for name, param in self.state_dict().items():
                console.log(f"[bold green]{name}, \n[yellow]{param.shape if shape_only else param}")
                console.rule()
        if info:
            title = f"[bold]Model configuration of [cyan]{self.name}, [green]type [cyan]{self.c.name}"
            print_dataclass_as_table(self.c, title)

    def get_param_scheme(self, state_dict: Dict = None):
        if state_dict is None:
            state_dict = self.state_dict()
        scheme = {
            "name": [],
            "shape": [],
            "chunk": []
        }
        for name, param in state_dict.items():
            scheme['name'].append(name)
            scheme['shape'].append(param.shape)
            scheme['chunk'].append(torch.numel(param))
        return scheme

    def get_param_scheme_in_groups(self, state_dict: Dict = None, group_name_list: List = None):
        if state_dict is None:
            state_dict = self.state_dict()
        scheme = dict(groups=group_name_list,
                      schemes=[dict(name=[], shape=[], chunk=[]) for _ in group_name_list],
                      group_chunk=[],
                      names=[], shapes=[], chunks=[])

        for name, param in state_dict.items():
            group_idx = [i for i in range(len(group_name_list)) if group_name_list[i] in name][0]
            scheme['schemes'][group_idx]['name'].append(name)
            scheme['schemes'][group_idx]['shape'].append(param.shape)
            scheme['schemes'][group_idx]['chunk'].append(torch.numel(param))

        for i in range(len(group_name_list)):
            scheme['names'].extend(scheme['schemes'][i]['name'])
            scheme['shapes'].extend(scheme['schemes'][i]['shape'])
            scheme['chunks'].extend(scheme['schemes'][i]['chunk'])
            scheme['group_chunk'].append(sum(scheme['schemes'][i]['chunk']))
        return scheme

    def get_param_vec_with_scheme(self, scheme):
        param_vec = []
        for name in scheme['name']:
            param_vec.append(torch.flatten(self.state_dict()[name]))
        return torch.cat(param_vec)

    def get_param_vec_with_scheme_in_groups(self, scheme, as_list: bool=False):
        param_vec = []
        for name in scheme['names']:
            param_vec.append(torch.flatten(self.state_dict()[name]))
        param_vec = torch.cat(param_vec)
        if as_list:
            return torch.split(param_vec, scheme['group_chunk'])
        else:
            return param_vec

    def load_state_dict_with_scheme(self, scheme, param_vec):
        state_dict = {}
        for name, shape, chunk in zip(scheme['name'], scheme['shape'], torch.split(param_vec, scheme['chunk'])):
            state_dict[name] = chunk.reshape(shape)
        self.load_state_dict(state_dict)

    def load_state_dict_with_scheme_in_groups(self, scheme, param_vec):
        state_dict = {}
        for name, shape, chunk in zip(scheme['names'], scheme['shapes'], torch.split(param_vec, scheme['chunks'])):
            state_dict[name] = chunk.reshape(shape)
        self.load_state_dict(state_dict)

    def get_total_num_params(self, trainable: bool = True):
        if trainable:
            return sum(p.numel() for p in self.parameters() if p.requires_grad)
        else:
            return sum(p.numel() for p in self.parameters())


class ModelFactory:
    """ The model factory class"""

    registry = {}  # type: Dict[str, Type[BaseModel]]
    """ Internal registry for available models """

    @classmethod
    def register(cls, name: str) -> Callable:
        """ Class method to register sub-class to the internal registry.
        Args:
            name (str): The name of the sub-class.
        Returns:
            The sub-class itself.
        """

        def inner_wrapper(wrapped_class: Type[BaseModel]) -> Type[BaseModel]:
            if name in cls.registry:
                user_warning(f'Model {name} already exists. Will replace it.')
            cls.registry[name] = wrapped_class
            return wrapped_class
        return inner_wrapper

    @classmethod
    def create_model(cls, name: str, config_file: Union[str, Path, Dict, None] = None, model_path: Path = None) -> BaseModel:
        """ Factory method to create the model with the given name and pass parameters as``kwargs``.
        Args:
            name (str): The name of the model to create.
            config_file:
            model_path:
        Returns:
            An instance of the model that is created.
        """
        console.log(f"[bold green]create model with name [cyan]{name}")
        if name not in cls.registry:
            user_warning(f'Model {name} does not exist in the registry, use {list(cls.registry.keys())} instead.')
            exit()

        model_class = cls.registry[name]
        model = model_class(config_file, model_path)
        return model

    @classmethod
    def create_model_from_path(cls, model_path: Path = None) -> BaseModel:
        d = load_dict_from_yaml(model_path / "meta_info.yaml")
        model_type = d['model_type']
        config_file = model_path / f"{model_type}_model_config.yaml"
        return ModelFactory.create_model(model_type, config_file, model_path)

    @classmethod
    def get_model_names(cls):
        return list(cls.registry.keys())

    @classmethod
    def get_model_types(cls):
        return [v.__class__.__name__ for v in cls.registry.values()]


@dataclass
class TrainConfig:
    batch_size:             int = 50
    batch_size_vali:        int = None
    max_epochs:             int = None
    max_iterations:         int = None
    default_max_epochs:     int = int(1e6)
    learning_rate:          float = 1e-3
    min_learning_rate:      float = 1e-6
    scheduler_for_epoch:    bool = True
    train_data_ratio:       float = 1.0
    gamma:                  float = 0.2
    save_model_epochs:      int = 10
    save_model_iterations:  int = 200
    validate_steps:         int = 500
    plot_train_process:     bool = False
    sample_batch_data_flag: bool = True
    clip_grad_norm:         float = 10.0
    patience:               int = 10
    cooldown:               int = 10

    epoch:                  int = 1
    iteration:              int = 1

    @property
    def name(self):
        return self.__class__.__name__


class BaseTrainer:
    def __init__(self,
                 model:             BaseModel,
                 config:            Union[str, Path, Dict, TrainConfig, None] = None,
                 loss_func:         Optional[Callable[..., torch.Tensor]] = None,
                 train_dataset:     torch.utils.data.Dataset = None,
                 test_dataset:      torch.utils.data.Dataset = None):
        console.rule(f"[bold cyan]Configuring Trainer")
        self.model = model
        self.device = model.device

        self.save_dir = self.model.model_path
        self.config_file = self.model.model_path / f"{self.model.name}_train_config.pt"
        self.config_file = self.model.model_path / f"{self.model.name}_train_config.yaml"
        if config is None:
            if not self.config_file.is_file():
                raise FileExistsError(f"You need to either "
                                      f"\n1) provide training config as argument or "
                                      f"\n2) make sure training config file {self.config_file} exists.")
            config = self.config_file
        self._load_config(config)
        title = f"[bold]Training Configuration of [cyan]{self.name}, [green]type: [cyan]{self.t.name}"
        print_dataclass_as_table(self.t, title)

        self._setup_training_dataset(train_dataset)
        self._setup_loss_fn(loss_func)
        self._setup_optimizer()

        self.monitor = dict(train=[], validate=[])
        self._train_state_dict_filename = self.model.model_path / f"train_state_dict.pt"

        if test_dataset:
            self._setup_testing(test_dataset)

        self._setup_logging()

    @property
    def name(self):
        return self.__class__.__name__

    def _save_config(self):
        dump_data(self.t, self.config_file)

    def _load_config(self, config: Union[str, Path, Dict, None]) -> None:
        self.t = load_dataclass(TrainConfig, config)

    def get_train_sampler(self, train_dataset):
        """
        train_dataset is the original training dataset
        """
        train_idx, validate_idx = split_indices(train_dataset, train_data_ratio=self.t.train_data_ratio)
        return BatchSampler(SubsetRandomSampler(train_idx), batch_size=self.t.batch_size, drop_last=False), validate_idx

    def _setup_training_dataset(self, train_dataset):
        if self.t.batch_size < 0:
            self.t.batch_size = len(train_dataset)
        train_sampler, validate_idx = self.get_train_sampler(train_dataset)
        self.train_dataloader = DataLoader(train_dataset, sampler=train_sampler)
        if validate_idx and hasattr(self.model, 'validate'):
            self.validate = True
            validate_batch_size = self.t.batch_size_vali if self.t.batch_size_vali else len(validate_idx)
            valid_sampler = BatchSampler(SubsetRandomSampler(validate_idx), batch_size=validate_batch_size, drop_last=False)
            self.validate_dataloader = DataLoader(train_dataset, sampler=valid_sampler)
        else:
            self.validate = False
            self.validate_dataloader = None

    def _setup_optimizer(self):
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.t.learning_rate)
        self.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            self.optimizer, patience=self.t.patience, cooldown=self.t.cooldown, min_lr=self.t.min_learning_rate,
            factor=self.t.gamma, verbose=False)

    def _setup_logging(self):
        self._logging_dir = get_validate_path(self.model.model_path / "log", create=True)
        # self.tensorboard = SummaryWriter(str(create_path(self.model.model_path / "log")))
        self.tensorboard = tensorboard_logger.Logger(
            get_validate_path(self._logging_dir / "tensorboard", create=True)
        )

        console.log(f"[bold green]"
                    f"logging dir: {self._logging_dir}\n"
                    f"see also \n\n"
                    f"'tensorboard' subdir, or run \n")
        console.log(f"[bold cyan]"
                    f"tensorboard --logdir {self.model.model_path.parent}\n\n")

        self.log_str = ""
        self.progress = Progress(console=console)
        self.log = Progress(TextColumn("{task.description}"), console=console)
        # self.progress_group = Group(self.progress, self.log)
        self.progress_group = Group(self.progress)

    def _setup_loss_fn(self, loss_func):
        if loss_func:
            self.loss_func = loss_func
        elif hasattr(self.model, 'loss_func'):
            self.loss_func = self.model.loss_func
        else:
            self.loss_func = None
            console.log("[red]Loss function is not defined when creating the model, \n"
                        "you should implement your own loss in the training process")

    def _setup_testing(self, test_dataset):
        sampler = BatchSampler(SequentialSampler(test_dataset), batch_size=len(test_dataset), drop_last=False)
        self.test_dataloader = DataLoader(test_dataset, sampler=sampler)

    def _get_loss(self, xt, yt, step):
        y, x = self.model(xt.to(self.device))
        if isinstance(y, tuple):
            y, pen_div = y[0], y[1]

        if isinstance(yt, torch.Tensor) and yt.shape[0] == 1:
            yt = yt[0]

        # loss = self.loss_func(yt.to(self.device), y, x)
        loss = self.loss_func(yt, y, x)
        return loss

    def _train_step(self, data, step, **kwargs):
        xt, yt = data
        self.optimizer.zero_grad(set_to_none=True)
        if xt.shape[0] == 1:
            xt = xt[0]

        loss = self._get_loss(xt, yt, step)
        loss.backward()
        torch.nn.utils.clip_grad_norm_(self.model.parameters(), self.t.clip_grad_norm)
        self.optimizer.step()

        if self.t.iteration % self.t.save_model_iterations == 0:
            self._save_model()

        if not self.t.scheduler_for_epoch:
            self.scheduler.step(loss)
        return loss

    def _save_model(self, best_param: bool = False):
        self.model.save_model(best_param)
        state = dict(
            optimizer=self.optimizer.state_dict(),
            scheduler=self.scheduler.state_dict()
        )
        torch.save(state, str(self._train_state_dict_filename))
        self._save_config()

    def _load_model(self):
        self.model.load_model()

        checkpoint = torch.load(self._train_state_dict_filename)  # type: dict
        self.optimizer.load_state_dict(checkpoint["optimizer"])
        self.scheduler.load_state_dict(checkpoint["scheduler"])

        self._load_config(self.config_file)
        console.log(f"[bold]Training config: {self.t}")

    def _setup_progress_tasks(
            self,
            additional_epochs: int = 0,
            additional_iterations: int = 0,
    ):
        self.flag_iter_prog = False
        if self.t.max_iterations is not None:
            self.t.max_epochs = self.t.default_max_epochs
            self.t.max_iterations += additional_iterations
            self.prog_task = self.progress.add_task("[blue]Training", total=self.t.max_iterations+1)
            self.log_task = self.log.add_task("", total=self.t.max_iterations+1)
            self.flag_iter_prog = True
        else:
            if self.t.max_epochs is None:
                self.t.max_epochs = self.t.default_max_epochs
            self.t.max_epochs += additional_epochs
            self.prog_task = self.progress.add_task("[blue]Training", total=self.t.max_epochs+1)
            self.log_task = self.log.add_task("", total=self.t.max_epochs+1)

        self.best_val_loss = 1e5
        self.best_param = None

        console.log(f"[bold green]start epoch: \t[cyan]{self.t.epoch}, "
                    f"\t[green]max epoch: [cyan]{self.t.max_epochs},\t"
                    f"[bold green]start Iteration: \t[cyan]{self.t.iteration}, "
                    f"\t[green]max Iteration: [cyan]{self.t.max_iterations},\t")
        console.rule("[bold blue]start training process")

    def _update_iter_progress(self):
        if self.flag_iter_prog:
            self.progress.update(self.prog_task, advance=1)

    def _update_eopch_progress(self):
        if not self.flag_iter_prog:
            self.progress.update(self.prog_task, advance=1)

    def train_model(
            self,
            reload: bool = False,
            additional_epochs: int = 0,
            additional_iterations: int = 0,
    ):
        """
        Training process. Adjust or overwrite if needed, especially _train_step() and/or _get_loss().

        Args:
            reload: if True, reload the checkpoints and resume the training process. The additional epochs below has
                to be set as well.
            additional_epochs: the additional epochs to train the model. Only valid when reload is True.
            additional_iterations: ... TODO
        """
        if reload:
            self._load_model()
        self.model.train()

        self._setup_progress_tasks(additional_epochs, additional_iterations)

        if not self.t.sample_batch_data_flag:
            data = next(iter(self.train_dataloader))

        with Live(self.progress_group):
            for step in range(self.t.epoch, self.t.max_epochs+1):
                epoch_loss, count = 0, 0
                self.t.epoch = step
                self.log_str = ""

                if self.t.sample_batch_data_flag:
                    for i, data in enumerate(self.train_dataloader):
                        if self.t.max_iterations and self.t.iteration >= self.t.max_iterations:
                            break
                        loss = self._train_step(data, step)
                        if loss is None:
                            continue
                        epoch_loss += loss
                        count += 1
                        self._update_iter_progress()
                else:
                    epoch_loss = self._train_step(data, step)
                    count = 1

                self._update_eopch_progress()
                train_loss = epoch_loss / count
                if self.t.scheduler_for_epoch:
                    self.scheduler.step(train_loss)

                if step % self.t.save_model_epochs == 0:
                    self._save_model()

                self._validate()
                self.log_str += f"epoch: {step:>7} loss={train_loss.item():>16.10f}"
                self.log.update(self.log_task, description=self.log_str)

                if self.t.max_iterations and self.t.iteration >= self.t.max_iterations:
                    break

            self.log.update(self.log_task, description=self.log_str + "\n")

            self._save_config()

    def update_plots(self, loss, **kwargs):
        learning_rate = self.optimizer.param_groups[0]['lr']
        self.tensorboard.log_value("learning rate", learning_rate, self.t.iteration)
        self.tensorboard.log_value(f"loss", loss.item(), self.t.iteration)

    def _validate(self):
        if not self.validate:
            return
        validate_loss = self.model.validate(self.validate_dataloader)
        if validate_loss < self.best_val_loss:
            self.best_val_loss = validate_loss
            self._save_model(best_param=True)

        self.tensorboard.log_value("validate loss", validate_loss, self.t.iteration)

    def test_model(self, *args, **kwargs):
        import matplotlib.pyplot as plt
        import csv
        import numpy as np
        # Note: this may not work out of box for your application,
        #  you should implement your own test code.
        self.model.eval()
        xt, yt = next(iter(self.test_dataloader))
        xt, yt = xt[0], yt[0]
        yt = yt.detach().cpu().numpy()

        ic(self.device, xt.device)
        y, x = self.model(xt.to(self.device))
        if isinstance(y, tuple):
            y = y[0]

        y = y.detach().cpu().numpy()

        saved_file = self.model.model_path / 'prediction_torch.csv'
        saved_figure = str(self.model.model_path / 'prediction_torch.png')
        with open(saved_file, 'w', newline='') as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerows(y)

        # t = np.arange(y.shape[0])
        t = xt.detach().cpu().numpy()
        n_plots = y.shape[1]
        fig = plt.figure()
        fig.subplots_adjust(hspace=0.000)
        for j in range(n_plots):
            ax = plt.subplot(n_plots, 1, j + 1)
            ic(type(yt))
            ax.plot(t, yt[:, j], 'k-', label='GT')
            ax.plot(t, y[:, j], 'r-.', label='Prediction')
            ax.legend()

            err = yt[:, j] - y[:, j]
            mse = np.mean(err ** 2)
            max_err = np.max(np.abs(err))
            console.log(f"the testing error: mse {mse}, max error: {max_err}")

        plt.tight_layout()
        plt.savefig(saved_figure, dpi=300)
        plt.show()

