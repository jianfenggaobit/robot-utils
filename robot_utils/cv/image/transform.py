import cv2
import numpy as np
import torch
from robot_utils import console
from torch.nn.functional import interpolate
import torchvision.transforms.functional as func


def crop_square_patch(
        image: np.ndarray,
        mask: np.ndarray,
        patch_resize: int = None,
        crop_mask: bool = False,
        ratio: float = 0.8,
):
    """
    Given a RGB image and a binary mask image of an object in the image, we crop a square image enclosing the obj.
    The cropped patch is centered at the center of the bounding box around the mask, adjustment is needed to respect
    the boundary of the image.

    Args:
        image: (h, w, 3)
        mask: (h, w)
        patch_resize: if given, the patch will be resized to the (patch_resize, patch_resize)
        crop_mask: if True, return the cropped mask image as well
        ratio: the crop ratio = the_size_of_the_raw_patch / size_of_the_resulting_patch

    Returns: tuple of (cropped_patch, mask, bounding_box: List[top_left_w, top_left_h, bottom_right_w, bottom_right_h])

    """
    # Find the coordinates of the non-zero pixels in the mask
    non_zero_coords = np.argwhere(mask > 0)

    # Compute the bounding box around the mask
    top_left = np.min(non_zero_coords, axis=0)
    bottom_right = np.max(non_zero_coords, axis=0)

    # Calculate width and height of the bounding box
    width = bottom_right[1] - top_left[1]
    height = bottom_right[0] - top_left[0]

    # Determine the side length of the square patch ensuring 80% of the mask is covered
    patch_size = min(round(max(width, height) / ratio), min(image.shape[:2]))

    # Calculate the center of the bounding box
    center_w = (top_left[1] + bottom_right[1]) // 2
    center_h = (top_left[0] + bottom_right[0]) // 2

    # Calculate the top-left corner of the square patch
    top_left_w = min(max(center_w - patch_size // 2, 0), image.shape[1] - patch_size)
    top_left_h = min(max(center_h - patch_size // 2, 0), image.shape[0] - patch_size)

    # Calculate the bottom-right corner of the square patch
    bottom_right_w = min(top_left_w + patch_size, image.shape[1])
    bottom_right_h = min(top_left_h + patch_size, image.shape[0])

    # Crop the square patch from the original image
    cropped_patch = image[top_left_h:bottom_right_h, top_left_w:bottom_right_w]

    if crop_mask:
        mask = mask[top_left_h:bottom_right_h, top_left_w:bottom_right_w]

    if patch_resize:
        cropped_patch = cv2.resize(cropped_patch, (patch_resize, patch_resize), interpolation=cv2.INTER_LANCZOS4)
        if crop_mask:
            mask = cv2.resize(mask, (patch_resize, patch_resize), interpolation=cv2.INTER_LANCZOS4)

    return cropped_patch, mask, (top_left_w, top_left_h, bottom_right_w, bottom_right_h)


def crop_square_patch_torch(
        img: torch.Tensor,
        mask: torch.Tensor,
        patch_resize: int = None,
        crop_mask: bool = False,
        uv: torch.Tensor = None
):
    """
    Given a RGB image and a binary mask image of an object in the image, we crop a square image enclosing the obj.
    The cropped patch is centered at the center of the bounding box around the mask, adjustment is needed to respect
    the boundary of the image.

    Args:
        img: (b, 3, h, w)
        mask: (b, h, w)
        patch_resize: if given, the patch will be resized to the (patch_resize, patch_resize)
        crop_mask: if True, return the cropped mask image as well
        uv: (b, N, 2) uv to be adapted to the new patch

    Returns: tuple of (cropped_patch, mask, bounding_box: List[top_left_w, top_left_h, bottom_right_w, bottom_right_h])

    """
    # Find the coordinates of the non-zero pixels in the mask
    image = img.squeeze(0)
    mask = mask.squeeze(0)
    non_zero_coords = torch.argwhere(mask)

    # Compute the bounding box around the mask
    top_left, _ = torch.min(non_zero_coords, dim=0)
    bottom_right, _ = torch.max(non_zero_coords, dim=0)
    # console.log(top_left, bottom_right)

    # Calculate width and height of the bounding box
    width = bottom_right[1] - top_left[1]
    height = bottom_right[0] - top_left[0]

    # Determine the side length of the square patch ensuring 80% of the mask is covered
    patch_size = min(torch.round(max(width, height) / 0.8).long(), min(image.shape[1:]))

    # Calculate the center of the bounding box
    center_w = (top_left[1] + bottom_right[1]) // 2
    center_h = (top_left[0] + bottom_right[0]) // 2

    # Calculate the top-left corner of the square patch
    top_left_w = min(max(center_w - patch_size // 2, 0), image.shape[2] - patch_size)
    top_left_h = min(max(center_h - patch_size // 2, 0), image.shape[1] - patch_size)

    # Calculate the bottom-right corner of the square patch
    bottom_right_w = min(top_left_w + patch_size, image.shape[2])
    bottom_right_h = min(top_left_h + patch_size, image.shape[1])

    # Crop the square patch from the original image
    # console.log(ic.format(image.shape, top_left_h, bottom_right_h, top_left_w, bottom_right_w))
    h, w = bottom_right_h-top_left_h, bottom_right_w-top_left_w
    if patch_resize:
        cropped_patch = func.resized_crop(image.unsqueeze(0), top_left_h, top_left_w, h, w, patch_resize, antialias=True)
        mask = func.resized_crop(mask.unsqueeze(0), top_left_h, top_left_w, h, w, patch_resize, antialias=True)
    else:
        cropped_patch = func.crop(image.unsqueeze(0), top_left_h, top_left_w, h, w)
        mask = func.crop(mask.unsqueeze(0), top_left_h, top_left_w, h, w)
    if uv is not None:
        uv[:, :, 0] = ((uv[:, :, 0] - top_left_w) * patch_resize / w).long()
        uv[:, :, 1] = ((uv[:, :, 1] - top_left_h) * patch_resize / h).long()

    return cropped_patch, mask, uv, (top_left_w, top_left_h, bottom_right_w, bottom_right_h)
