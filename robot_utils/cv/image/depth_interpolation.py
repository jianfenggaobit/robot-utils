import cv2
import numpy as np
import torch
import torch.nn.functional as F

# from scipy.ndimage import median_filter
from scipy.interpolate import NearestNDInterpolator
# from robot_utils.cv.io.io_cv import write_binary_mask
# from robot_utils.py.filesystem import get_validate_path


def fix_depth_interpolation(
        depth_map: np.ndarray,
        gradient_threshold_value: int = 50,
        median_filter_radius: int = -1
) -> np.ndarray:
    """
    Filtering the depth image that has interpolation from foreground to background. Result is a depth image
    of the same shape with sharp edges.

    Args:
        depth_map: (h, w) the depth image stored in millimeter.
        gradient_threshold_value: areas on the depth image, where the norm of the Sobel gradient is larger than
            this threshold, is counted as interpolation regions with wrong depth estimation.
        median_filter_radius: default -1, not enabled, otherwise, use median filter

    Returns: np.ndarray of the filtered depth image (h, w)

    """
    depth_image = depth_map.copy()
    gradient_x = cv2.Sobel(depth_image, cv2.CV_64F, 1, 0, ksize=3)
    gradient_y = cv2.Sobel(depth_image, cv2.CV_64F, 0, 1, ksize=3)
    gradient_magnitude = np.sqrt(gradient_x ** 2 + gradient_y ** 2)

    large_jump_mask = gradient_magnitude > gradient_threshold_value

    # path = get_validate_path("/tmp/fix_depth", create=True)
    # write_binary_mask(path / "jump_mask.png", large_jump_mask)

    # Note: find contour and indices
    inverted_mask = np.bitwise_not(large_jump_mask).astype(np.uint8)
    # write_binary_mask(path / "inverted_mask.png", inverted_mask)
    contours, _ = cv2.findContours(inverted_mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    contour_idx = np.ascontiguousarray(np.concatenate(contours, 0).squeeze()[:, ::-1])  # (Q, 2) in (h, w)

    # img = np.zeros_like(inverted_mask)
    # img[contour_idx[:, 0], contour_idx[:, 1]] = True
    # write_binary_mask(path / "contour.png", img)

    interp = NearestNDInterpolator(contour_idx, depth_image[contour_idx[:, 0], contour_idx[:, 1]])
    idx = np.where(large_jump_mask)
    z = interp(idx[0], idx[1])
    depth_image[idx[0], idx[1]] = z

    # Note: the cuda implementation is a bit slower (0.06215 > 0.0153) than NearestNDInterpolator above
    # contour_idx = torch.from_numpy(contour_idx).cuda()
    #
    # # Note: for each pixel in large_jump_mask, find its closest contour pixel
    # idx_pix_jump = torch.from_numpy(np.argwhere(large_jump_mask)).float().cuda()  # (P, 2) in (h, w)
    #
    # _, idx_closest_contour_pix = torch.norm(idx_pix_jump.unsqueeze(1) - contour_idx.unsqueeze(0), dim=-1).min(-1)
    # idx_closest_contour_pix = idx_closest_contour_pix.cpu().numpy()
    #
    # # Note: replace the depth values in the mask with nearest neighbors
    # contour_idx = contour_idx.cpu().numpy()
    # depth_image[large_jump_mask.astype(bool)] = depth_map[
    #     contour_idx[idx_closest_contour_pix, 0], contour_idx[idx_closest_contour_pix, 1]]

    # Note: apply median filter
    if median_filter_radius > 1:
        # Note: median_filter is slower (0.10271 > 0.03839) compared to torch implementation below
        # depth_image = median_filter(depth_image, size=median_filter_radius)
        depth_image = torch.from_numpy(depth_image).unsqueeze(0)
        depth_image = image_median_filter(depth_image, 5).squeeze().numpy()
    return depth_image


def image_median_filter(image: torch.Tensor, radius: int = 5):
    """

    Args:
        image: (C, H, W)
        radius:

    Returns: (C, H, W)

    """
    if image.ndim == 2:
        image = image.unsqueeze(0)
    return F.pad(
        image.unfold(-2, radius, 1).unfold(-2, radius, 1).flatten(-2).median(-1)[0],
        (radius // 2,) * 4, mode='replicate'
    ).squeeze()
