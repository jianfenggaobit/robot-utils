import torch
from typing import Optional, Union, Tuple

from pytorch3d.renderer import (
    FoVPerspectiveCameras,
    PerspectiveCameras,
    RasterizationSettings,
    MeshRasterizer,
    CamerasBase,
    ray_bundle_to_ray_points,
    NDCMultinomialRaysampler
)
from pytorch3d.structures import Pointclouds


class DepthRendererFoVPerspectiveCamera:
    """
    The FoVPerspectiveCameras will scale the object to NDC space, but it does not affect the
    true depth value. You can get the camera intrinsics from the returned camera instance.

    Args:
        mesh: a Meshes object representing a batch (N) of meshes
        R: Rotation matrix of shape (N, 3, 3)
        T: Translation matrix of shape (N, 3)
        image_size: Either common height and width or (height, width), in pixels.
        fov: the field of view

    Returns:
        a tuple of (depth map, cameras)

        - depth_map: (N, h, w, n_pixels = 1)
        - cameras: a batch of FoVPerspectiveCameras

    """
    def __init__(self, R, T, image_size, device, fov: int = 60):
        self.cameras = FoVPerspectiveCameras(device=device, R=R, T=T, fov=fov)
        raster_settings = RasterizationSettings(
            image_size=image_size,
            blur_radius=0.0,
            faces_per_pixel=1,
        )
        self.rasterizer = MeshRasterizer(cameras=self.cameras, raster_settings=raster_settings)

    def __call__(self, mesh):
        fragments = self.rasterizer(mesh)
        return fragments.zbuf.squeeze()


def render_depth_image_fov_perspective_camera(mesh, R, T, image_size, fov: int = 60):
    """
    The FoVPerspectiveCameras will scale the object to NDC space, but it does not affect the
    true depth value. You can get the camera intrinsics from the returned camera instance.

    Args:
        mesh: a Meshes object representing a batch (N) of meshes
        R: Rotation matrix of shape (N, 3, 3)
        T: Translation matrix of shape (N, 3)
        image_size: Either common height and width or (height, width), in pixels.
        fov: the field of view

    Returns:
        a tuple of (depth map, cameras)

        - depth_map: (N, h, w, n_pixels = 1)
        - cameras: a batch of FoVPerspectiveCameras

    """
    cameras = FoVPerspectiveCameras(device=mesh.device, R=R, T=T, fov=fov)

    raster_settings = RasterizationSettings(
        image_size=image_size,
        blur_radius=0.0,
        faces_per_pixel=1,
    )
    rasterizer = MeshRasterizer(cameras=cameras, raster_settings=raster_settings)
    fragments = rasterizer(mesh)

    return fragments.zbuf.squeeze(), cameras


def render_depth_image_perspective_camera(
        mesh,
        R,
        T,
        focal_length,
        principal_point,
        image_size: Union[int, Tuple[int, int]],
        in_ndc: bool = False):
    """

    Args:
        mesh: a Meshes object representing a batch (N) of meshes
        R: Rotation matrix of shape (N, 3, 3)
        T: Translation matrix of shape (N, 3)
        focal_length: Focal length of the camera in world units.
            A tensor of shape (N, 1) or (N, 2) for
            square and non-square pixels respectively.
        principal_point: xy coordinates of the center of
            the principal point of the camera in pixels.
            A tensor of shape (N, 2).
        image_size: Either common height and width or (height, width), in pixels.
        in_ndc: True if camera parameters are specified in NDC.
            If camera parameters are in screen space, it must
            be set to False.

    Returns:
        a tuple of (depth map, cameras)

        - depth_map: (N, h, w, n_pixels)
        - cameras: a batch of FoVPerspectiveCameras

    """
    if isinstance(image_size, int):
        cam_image_size = torch.ones((R.shape[0], 2), device=R.device) * image_size
    elif isinstance(image_size, tuple):
        cam_image_size = torch.tensor([image_size], device=R.device)
    elif isinstance(image_size, torch.Tensor):
        cam_image_size = image_size
        if image_size.ndim == 2:
            image_size = image_size[0]

    cameras = PerspectiveCameras(
        focal_length=focal_length, principal_point=principal_point, R=R, T=T,
        in_ndc=in_ndc, image_size=cam_image_size, device=mesh.device
    )

    raster_settings = RasterizationSettings(
        image_size=image_size,
        blur_radius=0.0,
        faces_per_pixel=1,
    )
    rasterizer = MeshRasterizer(cameras=cameras, raster_settings=raster_settings)
    fragments = rasterizer(mesh)

    return fragments.zbuf.squeeze(), cameras


def get_rgbd_point_cloud(
        camera: CamerasBase,
        depth_map: torch.Tensor,
        image_rgb: Optional[torch.Tensor] = None,
        mask: Optional[torch.Tensor] = None,
        mask_thr: float = 0.5,
        *,
        euclidean: bool = False,
) -> Pointclouds:
    """
    Given a batch of images, depths, masks and cameras, generate a single colored
    point cloud by unprojecting depth maps and coloring with the source
    pixel colors.

    Code adapted from
    https://github.com/facebookresearch/pytorch3d/blob/main/pytorch3d/implicitron/tools/point_cloud_utils.py

    Arguments:
        camera: Batch of N cameras
        image_rgb: Batch of N images of shape (N, C, H, W).
            For RGB images C=3.
        depth_map: Batch of N depth maps of shape (N, 1, H', W').
            Only positive values here are used to generate points.
            If euclidean=False (default) this contains perpendicular distances
            from each point to the camera plane (z-values).
            If euclidean=True, this contains distances from each point to
            the camera center.
        mask: If provided, batch of N masks of the same shape as depth_map.
            If provided, values in depth_map are ignored if the corresponding
            element of mask is smaller than mask_thr.
        mask_thr: used in interpreting mask
        euclidean: used in interpreting depth_map.

    Returns:
        Pointclouds object containing one point cloud.
    """
    batch, _, imh, imw = depth_map.shape

    # convert the depth maps to point clouds using the grid ray sampler
    pts_3d = ray_bundle_to_ray_points(
        NDCMultinomialRaysampler(
            image_width=imw,
            image_height=imh,
            n_pts_per_ray=1,
            min_depth=1.0,
            max_depth=1.0,
            unit_directions=euclidean,
        )(camera)._replace(lengths=depth_map[:, 0, ..., None])
    )

    pts_mask = depth_map > 0.0
    if mask is not None:
        pts_mask *= mask > mask_thr
    mask_counts = torch.cumsum(pts_mask.sum(dim=(1, 2, 3)), dim=0)
    pts_mask = pts_mask.reshape(-1)

    pts_3d = pts_3d.reshape(-1, 3)[pts_mask]
    if mask_counts[-1] != pts_3d.shape[0]:
        raise RuntimeError(f"the total masked pixels {mask_counts[-1]} "
                           f"does not match the shape of the pcl {pts_3d.shape}")

    if image_rgb is not None:
        pts_colors = torch.nn.functional.interpolate(
            image_rgb,
            size=[imh, imw],
            mode="bilinear",
            align_corners=False,
        )  # (b, c, h, w)
        pts_colors = pts_colors.permute(0, 2, 3, 1).reshape(-1, 3)[pts_mask]  # (b, hw*, 3)

    points, features = [], []
    for n in range(batch):
        start = 0 if n == 0 else mask_counts[n-1]
        points.append(pts_3d[start:mask_counts[n]])
        if image_rgb is not None:
            features.append(pts_colors[start:mask_counts[n]])

    if image_rgb is not None:
        return Pointclouds(points=points, features=features)
    return Pointclouds(points=points)
