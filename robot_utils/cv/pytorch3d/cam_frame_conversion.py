import torch

# see https://zhuanlan.zhihu.com/p/651937759
# see https://zhuanlan.zhihu.com/p/593204605


def pytorch3d_to_opencv(rotation: torch.Tensor, translation: torch.Tensor, as_matrix: bool = True):
    """
    Pytorch3d has a special convention: rotation matrix is the camera's orientation in the world frame, while the
    translation vector is to be applied to the object in the scene. Therefore, to recover the camera's frame in the
    world frame, you need to inverse the translation

    Args:
        rotation: Rotation matrix of shape (N, 3, 3)
        translation: Translation matrix of shape (N, 3)
        as_matrix: to convert to a (N, 3, 4) matrix

    Returns:
        either a Tuple(rotation, translation) or a (N, 3, 4) matrix.

    """
    rot = torch.einsum("nij,j->nij", rotation, torch.tensor([-1., -1, 1], device=rotation.device))
    t = - torch.einsum("nij,nj->ni", rotation, translation)
    if as_matrix:
        return torch.cat((rot, t.unsqueeze(-1)), dim=-1)
    return rot, t
