import torch
from pytorch3d.structures import Pointclouds, Meshes

from robot_utils.cv.pytorch3d.distance import point_mesh_face_distance


def distance_weighted_random_sample_points_in_mesh_bbox(
        mesh,
        num_points,
        scale_factor=1.5,
        min_probability: float = 0.1,
        decay_factor: float = 5.0,
        upsample_factor: int = 5.0,
):
    temp_num_points = upsample_factor * num_points
    points = uniform_sample_points_in_mesh_bbox(mesh, temp_num_points, scale_factor=scale_factor)

    # Compute the unsigned distances
    distances, _ = point_mesh_face_distance(mesh, Pointclouds(points.unsqueeze(0)), squared=False)

    # Compute probabilities based on distance
    probabilities = torch.clip(torch.exp(-decay_factor * distances), min_probability, 1.)
    sampled_points_mask = torch.rand((temp_num_points,), device=mesh.device) < probabilities

    n_in_mask = sampled_points_mask.sum()
    if n_in_mask < num_points:
        return points[sampled_points_mask], distances[sampled_points_mask]

    idx = torch.randperm(n_in_mask)[:num_points]
    return points[sampled_points_mask][idx], distances[sampled_points_mask][idx]


def uniform_sample_points_in_mesh_bbox(mesh: Meshes, num_points: int, scale_factor: float):
    # Get the bounding box for the mesh
    bounding_boxes = mesh.get_bounding_boxes()
    min_corners = bounding_boxes[..., 0]
    max_corners = bounding_boxes[..., 1]

    # Compute the center of the original bounding box
    bbox_center = 0.5 * (min_corners + max_corners)

    # Scale the bounding box
    new_bbox_range = scale_factor * (max_corners - min_corners)
    scaled_min_corners = bbox_center - 0.5 * new_bbox_range
    # scaled_max_corners = bbox_center + 0.5 * scale_factor * (max_corners - bbox_center)

    # Generate random points in the unit cube [0, 1]^3
    random_points = torch.rand((num_points, 3), device=mesh.device, dtype=mesh.verts_packed().dtype)

    # Scale and shift the points to fit within the scaled bounding box
    scaled_points = random_points * new_bbox_range + scaled_min_corners

    return scaled_points
