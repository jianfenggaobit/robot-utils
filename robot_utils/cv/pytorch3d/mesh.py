from typing import Tuple, Optional, List

import torch
from pytorch3d.renderer import TexturesVertex
from pytorch3d.structures import Meshes


def scale_mesh_to_batch(
        vertex: torch.Tensor,
        face: torch.Tensor,
        batch_size: int,
        sigma: float = 0.3,
        clip_range: Tuple[float, float] = (0.5, 2.0),
        scaling_factors: Optional[torch.Tensor] = None,
        device=None,
        first_mesh_unscaled: bool = False,
):
    """
    random scale the mesh into a batch of meshes. The scaling factors of each dimension (x, y, z) follow follow a
    Gaussian distribution and are independent, which are then clipped in the clip_range.

    Args:
        vertex: (N_vert, 3),
        face: (N_face, 3)
        batch_size: the number of random scaling factors
        sigma: the standard deviation of random factor distribution
        clip_range: the range of the sampled factor
        scaling_factors: Tensor of shape (N, 3) the scaling factors for batch of meshes.
            If specified, then batch_size, sigma and clip_range are not needed.
        device: torch device
        first_mesh_unscaled: if True, force the first mesh to be the original size

    Returns:
        a batch of randomly scaled meshes of the input data point. Two-element tuple

        - **scaled_vertices**: (batch, N_vert, 3)
        - **faces**: (batch, N_face, 3)

    """
    device = vertex.device if device is None else device
    if scaling_factors is not None:
        if scaling_factors.ndim != 2 or scaling_factors.shape[1] != 3:
            raise ValueError(f"the shape of scaling_factors must be (batch_size, 3), given is {scaling_factors.shape}")
        scaling_factors.to(device)
    else:
        # generate random scale uniform distribution
        # scaling_factors = 0.5 + 1.5 * torch.rand((batch_size, 3))

        # generate random scale gaussian distribution
        scaling_factors = torch.randn((batch_size, 3), device=device) * sigma + 1.0
        scaling_factors = torch.clamp(scaling_factors, clip_range[0], clip_range[1])
    if first_mesh_unscaled:
        scaling_factors[0] = 1.0

    # (1, N_v, 3) * (B, 1, 3) -> (B, N_v, 3)
    vertices = vertex.to(device).unsqueeze(0) * scaling_factors.unsqueeze(1)
    faces = torch.tile(face.to(device), (batch_size, 1, 1))

    return vertices, faces, scaling_factors


def random_scale_pytorch3d_mesh(
        mesh,
        batch_size: int,
        sigma: float = 0.3,
        clip_range: Tuple[float, float] = (0.5, 2.0)
):
    scaling_factors = torch.randn((batch_size, 3), device=mesh.device) * sigma + 1.0
    scaling_factors = torch.clamp(scaling_factors, clip_range[0], clip_range[1])
    return mesh.scale_verts(scaling_factors)


def creat_meshes_batch(vertices: torch.Tensor, faces: torch.Tensor, textures: torch.Tensor = None, device=None):
    """
    Create a batch of meshes given a batch of (vertices, faces, textures).

    Args:
        vertices: (batch, N_vert, 3)
        faces: (batch, N_face, 3)
        textures: (batch, N_vert, 3)

    Returns:
        a batch of meshes (Batch, )

    """
    device = vertices.device if device is None else device
    if vertices.ndim == 2:
        vertices = vertices.unsqueeze(0)
    if faces.ndim == 2:
        faces = faces.unsqueeze(0)
    if textures is None:
        textures = TexturesVertex(torch.ones_like(vertices, device=vertices.device))
    elif textures.ndim == 2:
        textures = textures.unsqueeze(0)
    return Meshes(verts=vertices, faces=faces, textures=textures).to(device)


# def create_mesh(vertex: torch.Tensor, face: torch.Tensor, textures)
#
#
# def create_meshes_from_list(vertex_list: List[torch.Tensor], face_list: torch.Tensor, texture_list: torch.Tensor = None):
#     mesh_list = []
#     from pathos.multiprocessing import Pool
#     import os
#     pool = Pool(os.cpu_count())
#     pool.map(Meshes, vertex_list, face_list, texture_list)

