import torch
import trimesh
from typing import Union
from pytorch3d.structures import Meshes
from robot_utils.py.filesystem import get_validate_path, Path


def save_pytorch3d_mesh(mesh: Meshes, filename: Union[str, Path]):
    filename = Path(filename)
    get_validate_path(filename.parent, create=True)

    vertices, faces = mesh.verts_packed(), mesh.faces_packed()
    tri_mesh = trimesh.Trimesh(vertices, faces)
    tri_mesh.export(str(filename))


def save_verts_faces(verts: torch.Tensor, faces: torch.Tensor, filename: Union[str, Path]):
    filename = Path(filename)
    get_validate_path(filename.parent, create=True)

    tri_mesh = trimesh.Trimesh(verts, faces)
    tri_mesh.export(str(filename))


