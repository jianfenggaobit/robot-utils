from pytorch3d.renderer.cameras import *


_BatchFloatType = Union[float, Sequence[float], torch.Tensor]


def camera_position_from_spherical_angles(
    distance: float,
    elevation: float,
    azimuth: float,
    degrees: bool = True,
    device: Device = "cpu",
    up_dir: str = "y",
) -> torch.Tensor:
    """
    Calculate the location of the camera based on the distance away from
    the target point, the elevation and azimuth angles.

    Adapted from pytorch3d.renderer.cameras. We use different definitions of the elevation and azimuth angles

    Args:
        distance: distance of the camera from the object.
        elevation, azimuth: angles.
            The inputs distance, elevation and azimuth can be one of the following
                - Python scalar
                - Torch scalar
                - Torch tensor of shape (N) or (1)
        degrees: bool, whether the angles are specified in degrees or radians.
        device: str or torch.device, device for new tensors to be placed on.
        up_dir: the up direction, options are ["x", "y", "z", "neg_x", "neg_y", "neg_z"]

    The vectors are broadcast against each other so they all have shape (N, 1).

    Returns:
        camera_position: (N, 3) xyz location of the camera.
    """
    broadcasted_args = convert_to_tensors_and_broadcast(
        distance, elevation, azimuth, device=device
    )
    dist, elev, azim = broadcasted_args
    if degrees:
        elev = math.pi / 180.0 * elev
        azim = math.pi / 180.0 * azim

    if up_dir == "z":
        x = torch.cos(elev) * torch.cos(azim)
        y = torch.cos(elev) * torch.sin(azim)
        z = torch.sin(elev)
    elif up_dir == "y":
        # x = torch.cos(elev) * torch.cos(azim)
        # z = torch.cos(elev) * torch.sin(azim)
        # y = torch.sin(elev)
        x = dist * torch.cos(elev) * torch.sin(azim)
        y = dist * torch.sin(elev)
        z = dist * torch.cos(elev) * torch.cos(azim)
    else:
        raise NotImplementedError

    camera_position = torch.stack([x, y, z], dim=1)
    if camera_position.dim() == 0:
        camera_position = camera_position.view(1, -1)  # add batch dim.
    return camera_position.view(-1, 3)


def look_at_rotation(
    camera_position, at=((0, 0, 0),), up=((0, 1, 0),), device: Device = "cpu"
) -> torch.Tensor:
    """
    This function takes a vector 'camera_position' which specifies the location
    of the camera in world coordinates and two vectors `at` and `up` which
    indicate the position of the object and the up directions of the world
    coordinate system respectively. The object is assumed to be centered at
    the origin.

    The output is a rotation matrix representing the transformation
    from **world coordinates -> view coordinates (the camera frame represented in world coordinates)**.

    **Note that, differently from pytorch3d, we adopt the opencv camera frame:
    x-axis pointing to right, y-axis pointing down, and z-axis pointing to the object/scene.**

    Args:
        camera_position: position of the camera in world coordinates
        at: position of the object in world coordinates
        up: vector specifying the up direction in the world coordinate frame.

    The inputs camera_position, at and up can each be a
        - 3 element tuple/list
        - torch tensor of shape (1, 3)
        - torch tensor of shape (N, 3)

    The vectors are broadcast against each other so they all have shape (N, 3).

    Returns:
        R: (N, 3, 3) batched rotation matrices
    """
    # Format input and broadcast
    broadcasted_args = convert_to_tensors_and_broadcast(
        camera_position, at, up, device=device
    )
    camera_position, at, up = broadcasted_args
    for t, n in zip([camera_position, at, up], ["camera_position", "at", "up"]):
        if t.shape[-1] != 3:
            msg = "Expected arg %s to have shape (N, 3); got %r"
            raise ValueError(msg % (n, t.shape))
    z_axis = F.normalize(at - camera_position, eps=1e-5)
    x_axis = F.normalize(torch.cross(z_axis, up, dim=1), eps=1e-5)
    # x_axis = F.normalize(torch.cross(up, z_axis, dim=1), eps=1e-5)
    y_axis = F.normalize(torch.cross(z_axis, x_axis, dim=1), eps=1e-5)
    is_close = torch.isclose(x_axis, torch.tensor(0.0), atol=5e-3).all(
        dim=1, keepdim=True
    )
    if is_close.any():
        replacement = F.normalize(torch.cross(y_axis, z_axis, dim=1), eps=1e-5)
        x_axis = torch.where(is_close, replacement, x_axis)
    R = torch.cat((x_axis[:, None, :], y_axis[:, None, :], z_axis[:, None, :]), dim=1)
    return R.transpose(1, 2)


def camera_view_transform(
    dist: _BatchFloatType = 1.0,
    elev: _BatchFloatType = 0.0,
    azim: _BatchFloatType = 0.0,
    degrees: bool = False,
    at=((0, 0, 0),),  # (1, 3)
    up=((0, 1, 0),),  # (1, 3)
    device: Device = "cpu",
    camera_convention = "opencv",
) -> Tuple[torch.Tensor, torch.Tensor]:
    """
    This function returns a rotation and translation matrix of the camera extrinsic

    Args:
        dist: distance of the camera from the object
        elev: angle in degrees or radians. This is the angle between the
            vector from the object to the camera, and the horizontal plane y = 0 (xz-plane).
        azim: angle in degrees or radians. The vector from the object to
            the camera is projected onto a horizontal plane y = 0.
            azim is the angle between the projected vector and a
            reference vector at (0, 0, 1) on the reference plane (the horizontal plane).
        dist, elev and azim can be of shape (1), (N).
        degrees: boolean flag to indicate if the elevation and azimuth
            angles are specified in degrees or radians.
        up: the direction of the x axis in the world coordinate system.
        at: the position of the object(s) in world coordinates.
        up and at can be of shape (1, 3) or (N, 3).
        camera_convention: the convention of defining the local frames of the camera. Default to opencv, options are
            [ 'opencv', 'opengl' ]

    Returns:
        2-element tuple containing

        - **R**: (Batch, 3, 3) the rotation matrix of the camera represented in world frame.
        - **T**: (Batch, 3) the position of the camera.

    References: code adapted from pytorch3d.renderer.cameras, we use z-up convention and adapted the definition of
        elevation and azimuth angles.
    """

    broadcasted_args = convert_to_tensors_and_broadcast(
        dist, elev, azim, at, up, device=device
    )
    dist, elev, azim, at, up = broadcasted_args
    C = (
        camera_position_from_spherical_angles(
            dist, elev, azim, degrees=degrees, device=device, up_dir="y"
        )
        + at
    )

    R = look_at_rotation(C, at, up, device=device)
    if camera_convention == "opengl":
        R[..., 0] *= -1
        R[..., 1] *= -1
    return R, C