import os
import time

import numpy as np
import torch
from torch.optim import Adam
from typing import Union

from scipy.spatial.distance import cdist
from scipy.optimize import minimize
from robot_utils import console
from robot_utils.cv.geom.pcl import get_pcl_spatial_scale
from robot_utils.math.transformations import euler_matrix
from robot_utils.serialize.dataclass import dataclass, load_dataclass, Path


def find_neighbors(
        point_cloud: torch.Tensor = None,
        num_neighbor_pts: int = None
) -> torch.Tensor:
    """
    Get the initial status of the local frame on canonical shape and sample some reference points around its origin,
    which will be used in later optimization step to find this local frame again on actual obj shape in the new scene.

    Args:
        point_cloud: (P, dim) the coordinates of all the sampled points on the object
        num_neighbor_pts: number of neighboring points to be considered

    Returns:
        indices of the `num_neighbor_pts` neighbors to each point (P, num_neighbor_pts)

    """
    if num_neighbor_pts is None:
        num_neighbor_pts = point_cloud.shape[0]

    return torch.argsort(
        (point_cloud.unsqueeze(dim=1) - point_cloud.unsqueeze(dim=0)).norm(dim=-1))[:, :num_neighbor_pts]


@dataclass
class PointCloudRepairConfig:
    """
    - kernel_alpha
        - 0.1 almost rigid
        - 0.5 almost linear to 0.7
        - 1 almost linear to 0.4
        - 4, almost linear to 0, ~0.5 at 0.5
        - 20, almost 0 at 0.5
        - 40, almost 0 at 0.4
        - 200, almost 0 at 0.15
    """
    threshold:      float = 0.05
    epochs:         int = 1000
    learning_rate:  float = 2.0
    kernel_alpha:   float = 0.1


class PointCloudRepair:
    def __init__(self, cfg: Union[str, Path, PointCloudRepairConfig] = None):
        self.c = load_dataclass(PointCloudRepairConfig, cfg)

        self.dist_profiles = None
        self.dist_profiles_diff = None
        self.template_ready = False
        self.current_idx_tuple = None

    def _reset_template(self, point_cloud: np.ndarray):
        self.template_pcl = pcl_torch = torch.from_numpy(point_cloud).float().cuda()
        self.spatial_scale = get_pcl_spatial_scale(point_cloud)
        distance = cdist(point_cloud, point_cloud, metric="euclidean")  # (N, N)
        distance = distance / self.spatial_scale
        self.distance = torch.from_numpy(distance).float().cuda().unsqueeze(0)

        self.weights = torch.exp(-self.c.kernel_alpha * self.distance ** 2)
        # Note: whether to normalize or not
        self.weights /= self.weights.sum(dim=-1, keepdim=True)

        # Note: furthest point sampling
        from pytorch3d.ops import sample_farthest_points
        selected_points, selected_idx = sample_farthest_points(pcl_torch.unsqueeze(0), K=50)
        selected_points = selected_points.squeeze()
        self.furthest_point_idx = selected_idx = selected_idx.squeeze()

        # Note: get neighborhood index in order (N, K), compute from self.distance
        self.pcl_furthest_neighbor_idx = torch.argsort(self.distance[:, :, selected_idx], dim=-1).squeeze()

        # Note: get neighborhood index of furthest points (K, K), compute from self.distance[K_idx, K_idx]
        self.furthest_point_neighbor_idx = torch.argsort(self.distance[:, selected_idx][:, :, selected_idx], dim=-1).squeeze()

        self.template = self._get_current_template(selected_points, pcl_torch)  # (N, K-1)

        self.template_ready = True
        self.prev_opt_pcl = None

    def clean_up(self):
        del self.template_pcl, self.distance, self.weights, self.spatial_scale
        del self.pcl_furthest_neighbor_idx, self.furthest_point_neighbor_idx
        del self.template, self.prev_opt_pcl

    def _get_current_template(
            self,
            furthest_point_xyz: torch.Tensor,
            points_of_interest: torch.Tensor,
            point_of_interest_idx: torch.Tensor = None
    ):
        if point_of_interest_idx is None:
            point_of_interest_idx = torch.arange(points_of_interest.shape[0])

        # Note: diff vec between K furthest points diff = (K, K, 3) respecting the neighboring index
        diff = furthest_point_xyz.unsqueeze(1) - furthest_point_xyz.unsqueeze(0)
        diff = diff[torch.arange(diff.size(0))[:, None], self.furthest_point_neighbor_idx]

        # Note: norm = diff[:, :-1] x diff[:, 1:] (K, K-1, 3)
        normal_vec = torch.cross(diff[:, :-1], diff[:, 1:], dim=-1)  # (K, K-1, 3)

        # Note: dot product/ projection of points on norm
        #  find the closed point in K to each normal point in pcl, the index vec = idx_closest_furthest
        #  dot(pcl, norm[idx_closest_furthest]) * self.weights
        #     (N, 3)  (N, K-1, 3) --> (N, K-1)  * (N, K-1)
        template = torch.einsum("ni,nki->nk",
                                points_of_interest,
                                normal_vec[self.pcl_furthest_neighbor_idx[point_of_interest_idx, 0]])
        # template = template * self.weights[0, :, selected_idx[:-1]]
        return template

    def with_template(self, point_cloud: np.ndarray):
        self._reset_template(point_cloud)
        return self

    def repair_occluded_points(
            self,
            trajectory: np.ndarray,
            init_point: np.ndarray = None
    ) -> np.ndarray:
        """

        Args:
            trajectory: when template as numpy array is given, trajectory can be of shape (T, N, 3) or (N, 3),
                with T >= 1. if template is an integer index, it takes the template-th frame of trajectory as template.
                Therefore, template: int < T.
                When template is not give, T must >= 2 and the first frame is used as template.
            # template: see above
            init_point: if provided, it will be used as initialization of the to-be-optimized points. Should have the
                same shape as the trajectory

        Returns: the trajectory with repaired points (T, N, 3)

        """
        console.rule("repair_occluded_points")
        start = time.time()
        if trajectory.ndim == 2:
            trajectory = trajectory[None, ...]

        if not self.template_ready:
            raise RuntimeError("You forget to set or reset template, "
                               "call 'with_template(pcl).repair_occluded_points(...)' instead")
        # T, N, _ = trajectory.shape
        # if template is None:
        #     if N < 2:
        #         raise ValueError("When template is not specified, the trajectory must have shape (T, N, 3), T >= 2")
        #     template_point_cloud = trajectory[0]
        # else:
        #     if isinstance(template, int):
        #         if template >= N:
        #             raise ValueError(f"template must be smaller than {N}, give {template}")
        #         template_point_cloud = trajectory[template]
        #     elif isinstance(template, np.ndarray):
        #         template_point_cloud = trajectory
        #     else:
        #         raise TypeError(f"type of template {type(template)} is not supported")
        #
        # self._reset_template(template_point_cloud)

        p_track_tensor = torch.from_numpy(trajectory).float().cuda()  # T, N, 3
        dist_profiles = torch.cdist(p_track_tensor, p_track_tensor, p=2) / self.spatial_scale  # (T, N, N)

        dist_profiles_diff = ((dist_profiles - self.distance).abs() * self.weights).sum(dim=-1)  # (T, N)
        self.current_idx_tuple = idx_tuple = torch.where(dist_profiles_diff > self.c.threshold)
        # ic(idx_tuple)  # ((M, ) for T dimension, (M, ) for N dimension), in total, M points

        if len(idx_tuple[0]) < 1:
            return trajectory

        if init_point is None:
            faulty_points = torch.nn.Parameter(
                p_track_tensor[idx_tuple].flatten(), requires_grad=True)
        else:
            faulty_points = torch.nn.Parameter(
                torch.from_numpy(init_point)[idx_tuple].flatten().float(), requires_grad=True)

        optimizer = Adam([faulty_points], lr=0.1)

        # Optimization loop
        # for step in range(epochs):
        #     optimizer.zero_grad()
        #     p = p_track_tensor.clone()
        #     p[idx_tuple] = faulty_points.view_as(p[idx_tuple])
        #     dist_profiles = torch.cdist(p, p, p=2) / spatial_scale  # (b, N, N)
        #     dist_profiles_diff = ((dist_profiles - distance).abs() * weights).sum(dim=-1)
        #     loss = dist_profiles_diff.mean()
        #     # console.log(f"[bold]Step: {step:>03d} -- Loss: {loss.item():>5.6f}")
        #
        #     # Backward and optimization step
        #     loss.backward()
        #     optimizer.step()
        #
        #     p_track_tensor[idx_tuple] = faulty_points.detach().clone().reshape(-1, 3)

        ground_truth_at_faulty = self.distance[0, idx_tuple[1]]  # (M, N)
        weights_at_faulty = self.weights[0, idx_tuple[1]]  # (M, N)
        # prev_faulty_point_idx = idx_tuple[0] - 1
        for step in range(self.c.epochs):
            optimizer.zero_grad()
            # prev_faulty_point = p_track_tensor[(prev_faulty_point_idx, idx_tuple[1])]
            traj_at_faulty = p_track_tensor[idx_tuple[0]]  # (M, N, 3)
            # (M, 1, 3) - (M, N, 3) --norm--> (M, N)
            dp = torch.norm(faulty_points.view(-1, 3).unsqueeze(1) - traj_at_faulty, dim=-1) / self.spatial_scale
            # ((M, N) - (M, N)) * (M, N) --sum--> (M, )
            dist_profiles_diff = ((dp - ground_truth_at_faulty).abs() * weights_at_faulty).sum(dim=-1)
            loss = dist_profiles_diff.mean()

            # loss += 0.05 * (prev_faulty_point - faulty_points.view(-1, 3)).norm(dim=-1).mean()
            # console.log(f"[bold]Step: {step:>03d} -- Loss: {loss.item():>5.6f}")

            # Backward and optimization step
            loss.backward()
            optimizer.step()

            p_track_tensor[idx_tuple] = faulty_points.detach().clone().reshape(-1, 3)

        dist_profiles = torch.cdist(p_track_tensor, p_track_tensor, p=2) / self.spatial_scale  # (T, N, N)
        self.dist_profiles_diff = ((dist_profiles - self.distance).abs() * self.weights).sum(dim=-1).cpu().numpy()  # (T, N)
        console.rule(f"repair_occluded_points: finished in {time.time() - start} second")
        return p_track_tensor.detach().cpu().numpy().squeeze()

    def repair_occluded_point_np(self, pcl: np.ndarray):
        """

        Args:
            pcl_init: (N, 3)
            pcl: (N, 3)

        Returns:

        """
        console.rule("repair_occluded_point_np")
        start = time.time()
        from scipy.spatial.distance import cdist
        from scipy.optimize import minimize

        pcl = pcl.copy()
        dist_ref = self.distance.squeeze(0).cpu().numpy()
        weight = self.weights.squeeze(0).cpu().numpy()
        dist_profiles = cdist(pcl, pcl, metric="euclidean") / self.spatial_scale        # (T, N, N)
        dist_profiles_diff = (np.abs(dist_profiles - dist_ref) * weight).sum(axis=-1)   # (T, N)
        idx = np.argwhere(dist_profiles_diff > self.c.threshold).squeeze()

        ic(dist_profiles.shape, dist_profiles_diff.shape, idx.shape, pcl.shape)

        position = pcl[idx].flatten()           # (M, 3)
        dist_gt_at_faulty = dist_ref[idx]       # (M, N)
        weights_at_faulty = weight[idx]         # (M, N)

        def obj(p):
            # (M, 1, 3) - (1, N, 3) --norm--> (M, N)
            pcl[idx] = p.reshape(-1, 3)
            dp = np.linalg.norm(p.reshape(-1, 3)[:, None] - pcl[None, ...], axis=-1) / self.spatial_scale
            # ((M, N) - (M, N)) * (M, N) --sum--> (M, ) --mean--> 1
            return (np.abs(dp - dist_gt_at_faulty) * weights_at_faulty).sum(axis=-1).mean()

        res = minimize(obj, position, method='SLSQP', tol=1e-6)
        pcl[idx] = res.x.reshape(-1, 3)

        console.rule(f"repair_pcl_rigid_np: finished in {time.time() - start} second")
        return pcl

    def repair_occluded_points_v2(
            self,
            trajectory: np.ndarray,
            init_point: np.ndarray = None
    ) -> np.ndarray:
        """

        Args:
            trajectory: when template as numpy array is given, trajectory can be of shape (T, N, 3) or (N, 3),
                with T >= 1. if template is an integer index, it takes the template-th frame of trajectory as template.
                Therefore, template: int < T.
                When template is not give, T must >= 2 and the first frame is used as template.
            # template: see above
            init_point: if provided, it will be used as initialization of the to-be-optimized points. Should have the
                same shape as the trajectory

        Returns: the trajectory with repaired points (T, N, 3)

        """
        if trajectory.ndim == 2:
            trajectory = trajectory[None, ...]

        if not self.template_ready:
            raise RuntimeError("You forget to set or reset template, "
                               "call 'with_template(pcl).repair_occluded_points(...)' instead")

        p_track_tensor = torch.from_numpy(trajectory).float().cuda()  # T, N, 3
        dist_profiles = torch.cdist(p_track_tensor, p_track_tensor, p=2) / self.spatial_scale  # (T, N, N)

        dist_profiles_diff = ((dist_profiles - self.distance).abs() * self.weights).sum(dim=-1)  # (T, N)
        self.current_idx_tuple = idx_tuple = torch.where(dist_profiles_diff > self.c.threshold)

        if len(idx_tuple[0]) < 1:
            return trajectory

        if init_point is None:
            faulty_points = torch.nn.Parameter(
                p_track_tensor[idx_tuple].flatten(), requires_grad=True)
        else:
            faulty_points = torch.nn.Parameter(
                torch.from_numpy(init_point)[idx_tuple].flatten().float(), requires_grad=True)

        optimizer = Adam([faulty_points], lr=0.05)
        # optimizer = Adam([faulty_points], lr=self.c.learning_rate)

        ground_truth_at_faulty = self.distance[0, idx_tuple[1]]  # (M, N)
        weights_at_faulty = self.weights[0, idx_tuple[1]]  # (M, N)

        ground_truth_template_at_faulty = self.template[idx_tuple[1]]  # (M, K-1)
        weights_template_at_faulty = self.weights[0, idx_tuple[1]][:, self.furthest_point_idx]  # (M, N)

        ic(ground_truth_at_faulty.shape, weights_at_faulty.shape, faulty_points.shape)
        for step in range(self.c.epochs):
            optimizer.zero_grad()
            # prev_faulty_point = p_track_tensor[(prev_faulty_point_idx, idx_tuple[1])]
            traj_at_faulty = p_track_tensor[idx_tuple[0]]  # (M, N, 3)
            ic(traj_at_faulty[0].shape, idx_tuple[1].shape, faulty_points.view(-1, 3).shape)
            template = self._get_current_template(traj_at_faulty[0, self.furthest_point_idx], faulty_points.view(-1, 3), idx_tuple[1])
            ic(template.shape)
            loss = (template - ground_truth_template_at_faulty).abs().mean()

            # (M, 1, 3) - (M, N, 3) --norm--> (M, N)
            dp = torch.norm(faulty_points.view(-1, 3).unsqueeze(1) - traj_at_faulty, dim=-1) / self.spatial_scale
            # ((M, N) - (M, N)) * (M, N) --sum--> (M, )
            dist_profiles_diff = ((dp - ground_truth_at_faulty).abs() * weights_at_faulty).sum(dim=-1)

            loss += dist_profiles_diff.mean()

            # loss += 0.05 * (prev_faulty_point - faulty_points.view(-1, 3)).norm(dim=-1).mean()
            # console.log(f"[bold]Step: {step:>03d} -- Loss: {loss.item():>5.6f}")

            # Backward and optimization step
            loss.backward()
            optimizer.step()

            p_track_tensor[idx_tuple] = faulty_points.detach().clone().reshape(-1, 3)

        dist_profiles = torch.cdist(p_track_tensor, p_track_tensor, p=2) / self.spatial_scale  # (T, N, N)
        self.dist_profiles_diff = ((dist_profiles - self.distance).abs() * self.weights).sum(dim=-1).cpu().numpy()  # (T, N)
        return p_track_tensor.detach().cpu().numpy().squeeze()

    def repair_occluded_point_np_v2(self, pcl: np.ndarray):
        """

        Args:
            pcl_init: (N, 3)
            pcl: (N, 3)

        Returns:

        """
        console.rule("repair_occluded_point_np_v2")
        start = time.time()
        from scipy.spatial.distance import cdist
        from scipy.optimize import minimize

        pcl = pcl.copy()
        dist_ref = self.distance.squeeze(0).cpu().numpy()
        weight = self.weights.squeeze(0).cpu().numpy()
        dist_profiles = cdist(pcl, pcl, metric="euclidean") / self.spatial_scale        # (T, N, N)
        dist_profiles_diff = (np.abs(dist_profiles - dist_ref) * weight).sum(axis=-1)   # (T, N)
        idx = np.argwhere(dist_profiles_diff > self.c.threshold).squeeze()

        ic(dist_profiles.shape, dist_profiles_diff.shape, idx.shape, pcl.shape)

        position = pcl[idx].flatten()           # (M, 3)
        dist_gt_at_faulty = dist_ref[idx]       # (M, N)
        weights_at_faulty = weight[idx]         # (M, N)

        template_gt_at_faulty = self.template[idx].cpu().numpy()  # (M, K-1)
        furthest_idx = self.furthest_point_idx.cpu().numpy()
        weights_template_at_faulty = weight[idx][:, furthest_idx]  # (M, N)

        def obj(p):
            # (M, 1, 3) - (1, N, 3) --norm--> (M, N)
            pcl[idx] = p.reshape(-1, 3)
            dp = np.linalg.norm(p.reshape(-1, 3)[:, None] - pcl[None, ...], axis=-1) / self.spatial_scale
            # ((M, N) - (M, N)) * (M, N) --sum--> (M, ) --mean--> 1
            loss = (np.abs(dp - dist_gt_at_faulty) * weights_at_faulty).sum(axis=-1).mean()

            template = self._get_current_template(
                torch.from_numpy(pcl[furthest_idx]).cuda(), torch.from_numpy(p.reshape(-1, 3)).cuda(), idx
            ).cpu().numpy()
            loss += np.abs(template - template_gt_at_faulty).mean() * 20.0
            return loss

        res = minimize(obj, position, method='SLSQP', tol=1e-6)
        pcl[idx] = res.x.reshape(-1, 3)

        console.rule(f"repair_occluded_point_np_v2: finished in {time.time() - start} second")
        return pcl

    def repair_pcl_rigid_torch_with_request_idx(
            self,
            pcl: np.ndarray,
            pcl_init: np.ndarray = None,
            requested_idx_for_repair: np.ndarray = None,
            start_from_prev_result: bool = False
    ):
        # console.log("[bold green] -- repair_pcl_rigid_torch")
        # start = time.time()

        if pcl.ndim == 2:
            pcl = pcl[None, ...]
            if isinstance(requested_idx_for_repair, np.ndarray) and requested_idx_for_repair.ndim == 1:
                requested_idx_for_repair = np.vstack(
                    (np.zeros_like(requested_idx_for_repair), requested_idx_for_repair)).T

        if not self.template_ready:
            raise RuntimeError("You forget to set or reset template, "
                               "call 'with_template(pcl).repair_occluded_points(...)' instead")

        if start_from_prev_result and self.prev_opt_pcl is not None:
            init_point = self.prev_opt_pcl

        if pcl_init is None:
            init_point = self.template_pcl
        else:
            init_point = torch.from_numpy(pcl_init).float().cuda()

        pcl = torch.from_numpy(pcl).float().cuda()  # T, N, 3

        if requested_idx_for_repair is None:
            dist_profiles = torch.cdist(pcl, pcl, p=2) / self.spatial_scale  # (T, N, N)
            dist_profiles_diff = ((dist_profiles - self.distance).abs() * self.weights).sum(dim=-1)  # (T, N)
            idx = torch.argwhere(dist_profiles_diff < self.c.threshold)
            to_be_repaired_idx = torch.argwhere(dist_profiles_diff >= self.c.threshold)
        else:
            to_be_repaired_idx = requested_idx_for_repair
            idx = np.ones(pcl.shape[:2])
            idx[to_be_repaired_idx[:, 0], to_be_repaired_idx[:, 1]] = 0
            idx = np.argwhere(idx.astype(bool))

        if len(to_be_repaired_idx) < 1:
            console.log("[bold yellow] -- Nothing to repair, exit")
            return pcl.cpu().numpy()

        # (T, 3) - (1, 3) -> (T, 3)
        trans = pcl.mean(dim=1) - pcl[0].mean(dim=0, keepdim=True)
        ori = torch.zeros_like(trans)
        transformation = torch.cat((trans, ori), dim=-1).float()      # (T, 6)
        transformation = torch.nn.Parameter(transformation.flatten(), requires_grad=True)  # (6*T, )

        optimizer = Adam([transformation], lr=0.005)

        from pytorch3d.transforms.rotation_conversions import euler_angles_to_matrix
        for step in range(500):
            optimizer.zero_grad()

            ori = transformation.view(-1, 6)[:, 3:]
            trans = transformation.view(-1, 6)[:, :3].unsqueeze(1)
            rot_mat = euler_angles_to_matrix(ori, "ZYX")
            points = torch.einsum("tij,nj->tni", rot_mat, init_point) + trans  # (T,N,3)

            loss = (pcl[idx[:, 0], idx[:, 1]] - points[idx[:, 0], idx[:, 1]]).norm(dim=-1).mean()
            console.log(loss.item())
            if loss < 0.018:
                break

            loss.backward()
            optimizer.step()

        self.prev_opt_pcl = points
        idx_t, idx_p = to_be_repaired_idx[:, 0], to_be_repaired_idx[:, 1]
        pcl[idx_t, idx_p] = points[idx_t, idx_p]
        # ic(points.shape, pcl.shape, to_be_repaired_idx)
        # console.log(f"[bold green] -- repair_pcl_rigid_torch: finished in {time.time() - start} second")
        console.rule()
        return pcl.detach().cpu().squeeze()

    def repair_pcl_rigid_torch(
            self,
            pcl: np.ndarray,
            pcl_init: np.ndarray = None,
            requested_idx_for_repair: np.ndarray = None,
    ):
        console.log("[bold green] -- repair_pcl_rigid_torch")
        start = time.time()

        if pcl.ndim == 2:
            pcl = pcl[None, ...]

        if not self.template_ready:
            raise RuntimeError("You forget to set or reset template, "
                               "call 'with_template(pcl).repair_occluded_points(...)' instead")

        if pcl_init is None:
            init_point = self.template_pcl
        else:
            init_point = torch.from_numpy(pcl_init).float().cuda()

        pcl = torch.from_numpy(pcl).float().cuda()  # T, N, 3
        dist_profiles = torch.cdist(pcl, pcl, p=2) / self.spatial_scale  # (T, N, N)

        dist_profiles_diff = ((dist_profiles - self.distance).abs() * self.weights).sum(dim=-1)  # (T, N)
        idx = torch.argwhere(dist_profiles_diff < self.c.threshold)
        to_be_repaired_idx = torch.argwhere(dist_profiles_diff >= self.c.threshold)
        # if requested_idx_for_repair is not None:
        #     to_be_repaired_idx = np.union1d(to_be_repaired_idx, requested_idx_for_repair)
        # idx = # TODO add reverse of to_be_repaired_idx
        # ic(dist_profiles.shape, dist_profiles_diff.shape, idx.shape, pcl.shape, to_be_repaired_idx)

        if len(to_be_repaired_idx) < 1:
            console.log("[bold yellow] -- Nothing to repair, exit")
            return pcl.cpu().numpy()

        # (T, 3) - (1, 3) -> (T, 3)
        trans = pcl.mean(dim=1) - pcl[0].mean(dim=0, keepdim=True)
        ori = torch.zeros_like(trans)
        transformation = torch.cat((trans, ori), dim=-1).float()      # (T, 6)
        transformation = torch.nn.Parameter(transformation.flatten(), requires_grad=True)  # (6*T, )

        optimizer = Adam([transformation], lr=0.005)

        from pytorch3d.transforms.rotation_conversions import euler_angles_to_matrix
        for step in range(500):
            optimizer.zero_grad()

            ori = transformation.view(-1, 6)[:, 3:]
            trans = transformation.view(-1, 6)[:, :3].unsqueeze(1)
            rot_mat = euler_angles_to_matrix(ori, "ZYX")
            points = torch.einsum("tij,nj->tni", rot_mat, init_point) + trans  # (T,N,3)

            loss = (pcl[idx[:, 0], idx[:, 1]] - points[idx[:, 0], idx[:, 1]]).norm(dim=-1).mean()

            loss.backward()
            optimizer.step()

        idx_t, idx_p = to_be_repaired_idx[:, 0], to_be_repaired_idx[:, 1]
        pcl[idx_t, idx_p] = points[idx_t, idx_p]
        # ic(points.shape, pcl.shape, to_be_repaired_idx)
        console.log(f"[bold green] -- repair_pcl_rigid_torch: finished in {time.time() - start} second")
        return pcl.detach().cpu().squeeze()

    def repair_pcl_rigid_np(
            self,
            pcl: np.ndarray,
            pcl_init: np.ndarray = None,
            start_from_prev_result: bool = False,
            requested_idx_for_repair: np.ndarray = None,
            index_mask: np.ndarray = None,
            debug: bool = False,
    ):
        if start_from_prev_result and self.prev_opt_pcl is not None:
            pcl_init = self.prev_opt_pcl

        if pcl_init is None:
            pcl_init = self.template_pcl.cpu().numpy()

        pcl_, points, idx_repaired = _repair_pcl_rigid_np(
            pcl, pcl_init, self.distance.cpu().numpy()[0], self.weights.cpu().numpy()[0],
            self.spatial_scale, self.c.threshold, requested_idx_for_repair, index_mask=index_mask, debug=debug
        )
        self.prev_opt_pcl = points
        return pcl_, idx_repaired

    def repair_pcl_rigid_np_batch(
            self,
            pcl_init: np.ndarray,
            pcl_traj: np.ndarray
    ):
        console.rule("repair_pcl_rigid_np_batch")
        start = time.time()
        from pathos.multiprocessing import Pool
        from functools import partial
        pool = Pool(os.cpu_count())

        dist_ref = self.distance.cpu().numpy()[0]
        weights = self.weights.cpu().numpy()[0]
        pcl_traj = np.array(pool.map(
            partial(_repair_pcl_rigid_np,
                    pcl_init=pcl_init, dist_ref=dist_ref, weights=weights,
                    spatial_scale=self.spatial_scale, threshold=self.c.threshold),
            pcl_traj
        ))
        console.rule(f"repair_pcl_rigid_np_batch: finished in {time.time() - start} second")
        return pcl_traj


def _repair_pcl_rigid_np(
        pcl: np.ndarray,
        pcl_init: np.ndarray,
        dist_ref: np.ndarray,
        weights: np.ndarray,
        spatial_scale: float,
        threshold: float,
        requested_idx_for_repair: np.ndarray = None,
        index_mask: np.ndarray = None,
        debug: bool = False
):
    # # pcl = pcl.copy()
    # if requested_idx_for_repair is None:
    #     dist_profiles = cdist(pcl, pcl, metric="euclidean") / spatial_scale  # (T, N, N)
    #     dist_profiles_diff = (np.abs(dist_profiles - dist_ref) * weights).sum(axis=-1)  # (T, N)
    #     idx = np.argwhere(dist_profiles_diff < threshold).flatten()
    #     to_be_repaired_idx = np.argwhere(dist_profiles_diff >= threshold).flatten()
    #     # console.log(f"to_be_repaired_idx: {len(to_be_repaired_idx)}, {len(idx)}, {pcl.min(axis=0)},{pcl.max(axis=0)}, {dist_profiles.shape}")
    # else:
    #     to_be_repaired_idx = requested_idx_for_repair
    #     idx = np.ones(pcl.shape[0])
    #     idx[to_be_repaired_idx] = 0
    #     idx = np.argwhere(idx.astype(bool)).squeeze()

    if index_mask is not None:
        mask = index_mask
    else:
        mask = np.ones(pcl.shape[0])
        mask[requested_idx_for_repair] = 0

    idx_kept = np.where(mask)[0]

    pcl_ = pcl[idx_kept]
    weights = weights[idx_kept][:, idx_kept]
    weights /= weights.sum(axis=-1, keepdims=True)
    dist_profiles = cdist(pcl_, pcl_, metric="euclidean") / spatial_scale  # (T, N, N)
    dist_profiles_diff = (np.abs(dist_profiles - dist_ref[idx_kept][:, idx_kept]) * weights).sum(axis=-1)  # (T, N)
    idx_out_of_profile = idx_kept[np.where(dist_profiles_diff >= threshold)[0]]

    mask[idx_out_of_profile] = 0
    idx = np.where(mask)[0]
    if len(idx) < 0.1 * pcl.shape[0]:
        idx = np.arange(pcl.shape[0])

    # (T, 3) - (1, 3) -> (T, 3)
    init_com = pcl_init[idx].mean(axis=0)
    translation = pcl[idx].mean(axis=0) - init_com
    orientation = np.zeros_like(translation)
    transformation = np.concatenate((translation, orientation), axis=-1)

    def obj(pose):
        rot_mat = euler_matrix(pose[3], pose[4], pose[5])[:3, :3]
        # console.log(ic.format(rot_mat.shape, pcl_init.shape, pose.shape, idx.shape))
        points = np.einsum("ij,nj->ni", rot_mat, pcl_init[idx]) + pose[:3]  # (N,3)
        loss = np.linalg.norm(pcl[idx] - points, axis=-1).mean()
        # loss += 0.001 * np.linalg.norm(points.mean(axis=0) - init_com)
        # loss += 0.001 * np.linalg.norm(pose[3:])
        # ic(f"{loss:>.5f}")
        return loss

    res = minimize(obj, transformation, method='SLSQP', tol=1e-6)
    pose = res.x

    rot_mat = euler_matrix(pose[3], pose[4], pose[5])[:3, :3]
    points = np.einsum("ij,nj->ni", rot_mat, pcl_init) + pose[:3]

    idx_repaired = np.where(mask == 0)[0]
    pcl[idx_repaired] = points[idx_repaired]
    # pcl = points
    return pcl, points, idx_repaired


def _repair_pcl_non_rigid_np(
        pcl: np.ndarray,
        pcl_init: np.ndarray,
        dist_ref: np.ndarray,
        weights: np.ndarray,
        spatial_scale: float,
        threshold: float,
        requested_idx_for_repair: np.ndarray = None,
):
    """

    Args:
        pcl_init: (N, 3)
        pcl: (N, 3)

    Returns:

    """
    # Try to normalize everything for opt and scale it back
    dist_profiles = cdist(pcl, pcl, metric="euclidean") / spatial_scale        # (T, N, N)
    dist_profiles_diff = (np.abs(dist_profiles - dist_ref) * weights).sum(axis=-1)   # (T, N)
    to_be_repaired_idx = np.argwhere(dist_profiles_diff > threshold).squeeze()
    ic(to_be_repaired_idx.shape)

    if requested_idx_for_repair is not None:
        to_be_repaired_idx = np.union1d(to_be_repaired_idx, requested_idx_for_repair)

    # idx = np.ones(pcl.shape[0])
    # idx[to_be_repaired_idx] = 0
    # idx = np.argwhere(idx.astype(bool)).squeeze()
    # if len(idx) < 0.1 * pcl.shape[0]:
    #     idx = np.arange(pcl.shape[0])

    pcl_ = pcl[to_be_repaired_idx].flatten()           # (M, 3)
    dist_ref_ = dist_ref[to_be_repaired_idx]       # (M, N)
    weights_ = weights[to_be_repaired_idx]         # (M, N)

    def obj(p):
        # (M, 1, 3) - (1, N, 3) --norm--> (M, N)
        pcl[to_be_repaired_idx] = p.reshape(-1, 3)
        dp = np.linalg.norm(p.reshape(-1, 3)[:, None] - pcl[None, ...], axis=-1) / spatial_scale
        # ((M, N) - (M, N)) * (M, N) --sum--> (M, ) --mean--> 1
        loss = (np.abs(dp - dist_ref_) * weights_).sum(axis=-1).mean()
        return loss

    res = minimize(obj, pcl_, method='SLSQP', tol=1e-6)
    pcl[to_be_repaired_idx] = res.x.reshape(-1, 3)
    return pcl, to_be_repaired_idx