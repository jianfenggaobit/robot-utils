import numpy as np
from scipy.spatial import ConvexHull
from scipy.spatial.distance import cdist


def get_pcl_spatial_scale(pcl: np.ndarray):
    hull = ConvexHull(pcl)
    hull_points = pcl[hull.vertices, :]  # Extract the points forming the hull
    spatial_scale = cdist(hull_points, hull_points, metric='euclidean').max()
    return spatial_scale


