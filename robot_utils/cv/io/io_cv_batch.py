from typing import List
from functools import partial
from pathos.multiprocessing import Pool

from robot_utils.cv.io.io_cv import *
from robot_utils.py.filesystem import get_ordered_files


def _prepare(
        path:       Union[str, Path],
        pattern:    List[str] = None,
        ex_pattern: List[str] = None,
        n_thread:   int = None,
):
    if n_thread is None:
        import os
        n_thread = os.cpu_count() - 1

    files = get_ordered_files(path, pattern, ex_pattern=ex_pattern)
    pool = Pool(n_thread)
    return pool, files


def load_rgb_batch(
        path:       Union[str, Path],
        pattern:    List[str] = None,
        ex_pattern: List[str] = None,
        bgr2rgb:    bool = False,
        n_thread:   int = None,
):
    pool, files = _prepare(path, pattern, ex_pattern, n_thread)
    images = pool.map(partial(load_rgb, bgr2rgb=bgr2rgb), files)
    return images, files


def load_mask_batch(
        path:       Union[str, Path],
        pattern:    List[str] = None,
        ex_pattern: List[str] = None,
        as_binary:  bool = False,
        n_thread:   int = None,
):
    pool, files = _prepare(path, pattern, ex_pattern, n_thread)
    images = pool.map(partial(load_mask, as_binary=as_binary), files)
    return images, files


def load_depth_batch(
        path:       Union[str, Path],
        pattern:    List[str] = None,
        ex_pattern: List[str] = None,
        to_meter:   bool = True,
        n_thread:   int = None,
):
    pool, files = _prepare(path, pattern, ex_pattern, n_thread)
    images = pool.map(partial(load_depth, to_meter=to_meter), files)
    return images, files


