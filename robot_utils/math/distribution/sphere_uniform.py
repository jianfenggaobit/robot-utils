import torch
import math


def sample_points_from_sphere_uniformly(n_samples: int):
    """
    The reference coordinate system: z-axis: up, x, y-axes form the horizontal plane.
    The elevation angle of a 3D point p = (x, y, z) is defined to be the angle between the vector p and the horizontal
    plane. positive elevation angles corresponds to the **angle of elevation** and negative values correspond to the
    **angle of depression**. And the azimuth angle corresponds to the angle between the projection of vector p on the
    horizontal plane and x-axis.

    Args:
        n_samples: number of sampled points

    Returns:
        two-element tuple

        - **elevation angle**: (batch, 1)
        - **azimuth angle**: (batch, 1)

    """
    # Sample elevation angle uniformly (-pi/2, pi/2)
    elevation = torch.acos(2 * torch.rand(n_samples) - 1) - torch.pi / 2.0
    # Sample azimuth angle uniformly (0, 2 * pi)
    azimuth = 2 * math.pi * torch.rand(n_samples)
    return elevation.reshape(-1, 1), azimuth.reshape(-1, 1)
