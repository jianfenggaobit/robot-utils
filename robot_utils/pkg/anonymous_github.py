import os
import json
import click
import requests
from time import sleep
import concurrent.futures
from robot_utils import console
from robot_utils.py.filesystem import Path, get_validate_path


def dict_parse(dic, pre=None):
    pre = pre[:] if pre else []
    if isinstance(dic, dict):
        for key, value in dic.items():
            if isinstance(value, dict):
                for d in dict_parse(value, pre + [key]):
                    yield d
            else:
                yield pre + [key, value]
    else:
        yield pre + [dic]


def req_url(dl_file, max_retry=5):
    url = dl_file[0]
    save_path = dl_file[1]
    save_dir = '/'.join(save_path.split('/')[:-1])
    if not os.path.exists(save_dir) and save_dir:
        try:
            os.makedirs(save_dir)
        except OSError:
            pass

    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.2 Safari/605.1.15"
    }
    for i in range(max_retry):
        try:
            r = requests.get(url, headers=headers)
            with open(save_path, "wb") as f:
                f.write(r.content)
            return
        except Exception as e:
            print('file request exception (retry {}): {} - {}'.format(i, e, save_path))
            sleep(0.4)


@click.command(context_settings=dict(help_option_names=['-h', '--help']))
@click.option("--url", "-u",        type=str, default=None, help="github url")
@click.option("--save_path", "-p",  type=Path, default=None, help="the absolute path where the cloned repo will be saved")
@click.option("--name", "-n",       type=str, default=None, help="the name of the project, defaults to anonymous github project name")
@click.option("--max_conns", "-c",  type=int, default=128, help="the name of the project, defaults to anonymous github project name")
def download_anonymous_github(url, save_path, name, max_conns):
    split_url = url.split('/')
    idx_git_repo_name = split_url.index("r") + 1
    git_name = split_url[idx_git_repo_name]
    console.log(f'git_name: {git_name}')
    if name is None:
        name = git_name
    proj_path = get_validate_path(save_path / name, create=True)

    dl_url = "https://anonymous.4open.science/api/repo/" + git_name + "/files/"
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.2 Safari/605.1.15"
    }
    console.log(f'downloading anonymous github repo: {dl_url}')

    resp = requests.get(url=dl_url, headers=headers)
    if resp.status_code == 200:
        try:
            file_list = resp.json()
        except json.JSONDecodeError:
            console.log(f"[bold red]Error decoding JSON: Response content is not valid JSON\n{resp.content}")
    else:
        console.log(f"[bold red]Error connecting to server: Code={resp.status_code}")
        exit()

    files = []
    out = []
    for file in dict_parse(file_list):
        file_path = os.path.join(*file[-len(file):-2])  # * operator to unpack the arguments out of a list
        save_path = proj_path / file_path
        file_url = dl_url + file_path
        files.append((file_url, save_path.as_posix()))

    with concurrent.futures.ThreadPoolExecutor(max_workers=max_conns) as executor:
        future_to_url = (executor.submit(req_url, dl_file) for dl_file in files)
        for future in concurrent.futures.as_completed(future_to_url):
            try:
                data = future.result()
            except Exception as exc:
                data = str(type(exc))
            finally:
                out.append(data)

    console.log(f"files saved to: {proj_path}")


if __name__ == '__main__':
    download_anonymous_github()
