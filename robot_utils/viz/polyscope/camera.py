import time
import numpy as np
import polyscope as ps
from polyscope import imgui as psim

from robot_utils import console


class PolyscopeCameraOrbit:
    """
    Animate the camera motion following an orbit.
    """
    def __init__(
            self,
            camera_pos_trajectory,
            look_at_trajectory,
            time_duration_sec: float = 5,
            loop: bool = False,
            smooth: bool = True,
    ):
        """
        Args:
            camera_pos_trajectory: (N, 3) the trajectory of the camera
            look_at_trajectory: (N, 3) the position in the scene that the camera is looking at
            time_duration_sec: the time duration of the orbit motion for one period
            loop: whether to loop the camera orbit motion
            smooth: whether to set smooth transition from one viewpoint to another, this will also affect the reset and
                home position.
        """
        self.camera_pos_trajectory = camera_pos_trajectory
        self.look_at_trajectory = look_at_trajectory
        self.max_steps = self.camera_pos_trajectory.shape[0] - 1
        self.loop = loop
        self.smooth = smooth
        self.step = 0
        self.prev_time = time.time()
        self.delta_time = time_duration_sec / self.max_steps
        self.started = False

    def start(self):
        console.rule("start camera orbit")
        self.reset()
        self.prev_time = time.time()
        self.started = True

    def stop(self):
        self.started = False
        console.log("camera orbit stopped")

    def reset(self):
        console.log("reset camera orbit to start point")
        self.step = 0
        ps.look_at(self.camera_pos_trajectory[self.step], self.look_at_trajectory[self.step], fly_to=True)

    @staticmethod
    def set_to_home():
        ps.reset_camera_to_home_view()

    def forward(self):
        """
        You need to call this method in your user-callback function, refer to 'default_monitor()' method for an example.
        """
        if not self.started:
            return
        if time.time() - self.prev_time < self.delta_time:
            return
        self.prev_time = time.time()
        self.step = min(self.max_steps, self.step + 1)
        ps.look_at(self.camera_pos_trajectory[self.step], self.look_at_trajectory[self.step], fly_to=self.smooth)
        if self.loop and self.step == self.max_steps:
            self.step = 0

    def default_monitor(self):
        """
        A default GUI set up, with animate, stop, reset button on the same row
        """
        psim.Separator()
        psim.Text("Camera Orbit Control")
        if psim.Button("animate"):
            self.start()
        psim.SameLine()
        if psim.Button("stop"):
            self.stop()
        psim.SameLine()
        if psim.Button("reset"):
            self.reset()

        psim.SameLine()
        if psim.Button("stop loop" if self.loop else "loop"):
            self.loop = not self.loop

        psim.SameLine()
        if psim.Button("stop smooth" if self.smooth else "smooth"):
            self.smooth = not self.smooth
        psim.Separator()
        self.forward()

    @classmethod
    def set_default_orbit_trajectory(
            cls,
            up_dir: str,
            cam_to_origin_distance: float,
            time_duration_sec: float = 5,
            loop: bool = False,
            smooth: bool = True,
    ):
        elevation = 30 * np.pi / 180.
        azimuth = np.linspace(0, np.pi * 2, 100)
        y = cam_to_origin_distance * np.sin(elevation) * np.ones_like(azimuth)
        x = cam_to_origin_distance * np.cos(elevation) * np.cos(azimuth)
        z = cam_to_origin_distance * np.cos(elevation) * np.sin(azimuth)
        cam_position = np.column_stack((x, y, z))

        order = [0, 1, 2]
        if up_dir == "z_up":
            order = [0, 2, 1]

        cam_position = cam_position[:, order]
        look_at_trajectory = np.zeros_like(cam_position)
        return PolyscopeCameraOrbit(cam_position, look_at_trajectory, time_duration_sec, loop, smooth)
