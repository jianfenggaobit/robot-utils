import numpy as np
import polyscope as ps


def viz_bbox(
        bbox_min: np.array,
        bbox_max: np.ndarray = None,
        bbox_dimension: np.ndarray = None,
        label: str = "",
        radius: float = 0.01,
        transparency: float = 0.5,
):
    bbox_min_max = np.column_stack((bbox_min, bbox_max))
    corners_points = np.array(np.meshgrid(bbox_min_max[0], bbox_min_max[1], bbox_min_max[2])).T.reshape(-1, 3)
    corners = ps.register_point_cloud(
        f"bbox_corner_{label}", corners_points, enabled=True, radius=radius
    )
    corners.set_radius(radius, False)
    corners.set_transparency(transparency)

    edge = np.array([[0, 1], [1, 3], [3, 2], [2, 0], [4, 5], [5, 7], [7, 6], [6, 4], [0, 4], [1, 5], [2, 6], [3, 7]])
    edge = ps.register_curve_network(
        f"bbox_edge_{label}", corners_points, edge, enabled=True, radius=radius * 0.8
    )
    edge.set_radius(radius * 0.5, False)
    edge.set_transparency(transparency)
    return corners, edge

