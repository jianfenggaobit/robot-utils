# Normally, you need to login to your NVIDIA account to download the compressed package file and
# extract it to the working direction, but here you can use the git repo I prepared in our GitLab instance

cd $PROJECT_PATH_CONTROL
git clone git@git.h2t.iar.kit.edu:sw/machine-learning-control/isaacgym.git
cd $PROJECT_PATH_CONTROL/isaacgym/python
pip install -e .

# carefully refine the following, because some of the strings (LD_LIBRARY_PATH) should remain as is
# but others ($CONDA_ENV_NAME) need to be parsed
echo "##### ISaacGym ######" >> $HOME/.bashrc
echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/opt/anaconda3/envs/$CONDA_ENV_NAME/lib" >> $HOME/.bashrc

cd $PROJECT_PATH_CONTROL
git clone git@github.com:NVIDIA-Omniverse/IsaacGymEnvs.git isaacgym-envs