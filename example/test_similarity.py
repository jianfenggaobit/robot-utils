import time

import numpy as np
import torch
from robot_utils.torch.torch_utils import get_device

from torch.nn.functional import cosine_similarity as cos_sim_t


def cosine_similarity_matching_rows(a: np.array, b: np.array, wa: np.ndarray = None, wb: np.ndarray = None, eps=1e-8):
    """
    compute the similarity of batches of vectors between a and b that has matching number rows

    Args:
        a: (batch, dim)
        b: (batch, dim)
        wa: (batch_b, ) weights of a
        wb: (batch_b, ) weights of b
        eps: for numerical stability

    Returns: the similarity matrix (batch, )

    """
    a_n, b_n = np.linalg.norm(a, axis=1)[:, None], np.linalg.norm(b, axis=1)[:, None]
    a_norm = a / np.where(a_n < eps, eps, a_n)
    b_norm = b / np.where(b_n < eps, eps, b_n)
    sim_matrix = np.einsum("ni,ni->n", a_norm, b_norm)
    if wa is not None and wb is not None:
        w = wa * wb
        sim_matrix = (sim_matrix * w).sum() / w.sum()
    return sim_matrix


na, nb = 2, 3
device = get_device(True)
a_np = [np.random.random((113, 2)) for _ in range(na)]
b_np = [np.random.random((113, 2)) for _ in range(nb)]
wa_np = [np.random.random(113) for _ in range(na)]
wb_np = [np.random.random(113) for _ in range(nb)]

start = time.time()
similarity_matrix = np.zeros((na, nb))

for i in range(na):
    for j in range(nb):  # Compute only upper triangular portion (including diagonal)
        similarity = cosine_similarity(a_np[i], b_np[j], wa_np[i], wb_np[j])  # Compute similarity between arrays
        similarity_matrix[i, j] = similarity

ic(time.time() - start)
ic(similarity_matrix)


start = time.time()
a_t = [torch.from_numpy(a).to(device).float() for a in a_np]
b_t = [torch.from_numpy(b).to(device).float() for b in b_np]
wa_t = [torch.from_numpy(a).to(device).float() for a in wa_np]
wb_t = [torch.from_numpy(b).to(device).float() for b in wb_np]

similarity_matrix = torch.zeros((na, nb), device=device, dtype=torch.float32)

for i in range(na):
    for j in range(nb):  # Compute only upper triangular portion (including diagonal)
        similarity = cos_sim_t(a_t[i], b_t[j])
        w = wa_t[i] * wb_t[j]
        similarity = (similarity * w).sum() / w.sum()
        similarity_matrix[i, j] = similarity

ic(time.time() - start)
ic(similarity_matrix)