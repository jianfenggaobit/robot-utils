import marshmallow
import numpy as np
import torch

from robot_utils.serialize.dataclass import dump_data_to_yaml, default_field, load_dataclass, merge, load_dict_from_yaml
from robot_utils.serialize.schema_numpy import NumpyArray
from robot_utils.serialize.schema_torch import TorchTensor
from marshmallow_dataclass import dataclass
from marshmallow import Schema, fields


@dataclass
class TestClass:
    name: str = "test"
    pose: NumpyArray = default_field(np.zeros((2, 3), dtype=float))
    vel: TorchTensor = default_field(torch.ones((3, 4), dtype=torch.long))


class ConfigurationSchema(Schema):
    class Meta:
        # Exclude attributes not present in the input data
        exclude = set(TestClass.__dataclass_fields__.keys()) - set(user_config_data.keys())


t = TestClass
p = "./test.yaml"
dump_data_to_yaml(TestClass, t, p)
d = load_dataclass(TestClass, p)
d.vel = torch.zeros((3, 4), dtype=torch.long)
ic(d)

user_dict = load_dict_from_yaml("./user.yaml")
schema = TestClass.Schema(partial=True)
dd = schema.load(user_dict, unknown=marshmallow.EXCLUDE)
ic(dd, d)

