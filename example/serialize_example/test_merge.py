import yaml
import robot_utils
from marshmallow import Schema, fields
from marshmallow_dataclass import dataclass, dataclass_from_dict


def generate_schema(data_class):
    fields_dict = {}

    for field_name, field_type in data_class.__annotations__.items():
        if field_type in (str, int, float, bool):
            fields_dict[field_name] = fields.Field()
        elif field_type in (list, set):
            inner_type = data_class.__dataclass_fields__[field_name].type.__args__[0]
            fields_dict[field_name] = fields.List(fields.Nested(generate_schema(inner_type)))
        else:
            fields_dict[field_name] = fields.Nested(generate_schema(field_type))

    return type(f"{data_class.__name__}Schema", (Schema,), fields_dict)


@dataclass
class Address:
    street: str
    city: str


@dataclass
class Person:
    name: str
    age: int
    address: Address


# Load user configuration from YAML
with open("./user_config.yaml", "r") as file:
    user_config_data = yaml.safe_load(file)

# Generate the Marshmallow schema for Person
PersonSchema = generate_schema(Person)

# Deserialize the user configuration using the schema
person_schema = PersonSchema()
merged_config = person_schema.load(user_config_data, partial=True)

# Access the merged configuration
ic(merged_config)
ic(type(merged_config))
print(merged_config.name)
print(merged_config.age)
print(merged_config.address.street)
print(merged_config.address.city)
