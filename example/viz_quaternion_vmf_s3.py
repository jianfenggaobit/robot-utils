import polyscope as ps
import open3d as o3d
import numpy as np
import torch
import matplotlib.pyplot as plt
import robot_utils.math.transformations as t
# from skspatial.objects import Sphere
from robot_utils.math.distribution.von_mises_fisher import VonMisesFisher
from robot_utils.math.transformations_torch import quaternion_average
from robot_utils.math.manifold.operator_np import sphere_logarithmic_map
from robot_utils.math.viz.polyscope import plot_quat, plot_two_sphere, plot_vec_field, plot_quat_traj
from robot_utils.py.visualize import set_3d_equal_auto, draw_frame_3d, set_3d_ax_label
# from salat.utils.utils import multivariate_normal, get_covariance
ps.init()
ps.set_up_dir("z_up")
ps.set_ground_plane_mode("shadow_only")
ps.add_scene_slice_plane()


local_traj = np.load("./local_traj.npy")
batch, dim, time = local_traj.shape
ic(local_traj.shape)

use_traj: bool = False
show_double_cover: bool = False

# np.savetxt("./local_traj_0.csv", local_traj[0], delimiter=",", fmt="%.4f")
loc = quaternion_average(torch.from_numpy(local_traj.transpose((0, 2, 1))).reshape(-1, 4))
ic(loc.shape)

scale = torch.ones(1) * 25.0
vmf = VonMisesFisher(loc=loc, scale=scale, k=1)

if use_traj:
    data = local_traj.transpose((0, 2, 1)).reshape(-1, 4)
else:
    data = vmf.sample(10000).numpy()
nb_data, nb_dim = data.shape

lh = torch.exp(vmf.log_prob(data))
color_idx = (lh - lh.min())/(lh.max() - lh.min())

s_x = np.zeros((nb_data, 4))
for i in range(nb_data):
    s_x[i] = t.quaternion_rotate(np.array([0, 1., 0., 0.]), data[i])

xts = np.zeros((nb_data, nb_dim))
# xts = sphere_logarithmic_map(data, average_quat.numpy())
for n in range(nb_data):
    d = data[n].flatten()
    # if isinstance(d, torch.Tensor):
    #     d = d.numpy()
    xts[n, :] = sphere_logarithmic_map(d, loc.numpy().flatten()).flatten()

covariances = np.identity(4, dtype=float) * 0.1
# covariances = get_covariance(4)
ic(covariances)
# w, v = np.linalg.eigh(covariances)
# ic(w, v)
# proj_matrix = np.identity(4, dtype=float) - np.einsum("i,j->ij", loc, loc)
# ic(proj_matrix)
# covariances = proj_matrix.dot(v)
# ic(covariances)

# prob = multivariate_normal(xts, np.zeros_like(loc.numpy()), covariances, log=False)
prob = torch.exp(vmf.log_prob(torch.from_numpy(data))).numpy()
# np.savetxt("./prob.csv", prob.reshape(batch, time), delimiter=",", fmt="%.6f")

color_spec = plt.get_cmap("plasma")
colors = color_spec(np.linspace(0, 1, batch))
# x_trajs = []
# for idx_trial in range(local_traj.shape[0]):
#     # ic(idx_trial, np.linalg.norm(local_traj[idx_trial, :, :], axis=0))
#     x_traj = np.array([
#         t.quaternion_rotate(np.array([0, 1., 0., 0.]), local_traj[idx_trial, :, time])
#         for tt in range(time)
#     ])
#     x_trajs.append(x_traj)
#     if idx_trial == 0:
#         ax.plot(x_traj[:, 1], x_traj[:, 2], x_traj[:, 3], color=colors[idx_trial], linestyle="--", alpha=0.8)
# x_trajs = np.array(x_trajs)
# x_trajs = x_trajs.reshape(-1, 4)


# ================================ plot =======================================
# ----------------------------- sphere mesh -----------------------------------
plot_two_sphere()

# ----------------------------- plot points -----------------------------------
plot_quat(data, double_cover=show_double_cover, colors=color_spec(color_idx)[:, :3], name="test")

# ----------------------------- plot mean -----------------------------------
mean_pos = loc.numpy()[np.newaxis, ...]
mean_pos = np.concatenate([mean_pos, -mean_pos], axis=0)
plot_quat(mean_pos, show_double_cover, np.tile(np.ones(3), (2, 1)), name="mean", radius=0.02)

# ----------------------------- plot traj -----------------------------------
cmap = plt.get_cmap("plasma")(np.linspace(0, 1, local_traj.shape[-1]))
for i, traj in enumerate(local_traj):
    # plot_quat(traj.transpose(1, 0), False, cmap[:, :3], name=f"traj_{i}", radius=0.01)
    plot_quat_traj(traj.transpose(1, 0), False, cmap[:, :3], name=f"traj_{i}", radius=0.01)

# ----------------------------- plot vector field -----------------------------------

# from robot_utils.math.manifold.sphere import Sphere
# s3 = Sphere()
# random_quat = s3.rand_uniform((2000, 4))
#
# colors_rand = color_spec(np.linspace(0, 1, random_quat.shape[0]))
# dq = torch.nn.functional.normalize(s3.log(random_quat, loc)).numpy()
# hemi1, hemi2, idx_1, idx_2 = plot_vec_field(
#     random_quat, vec=dq, colors=colors_rand, name="rand", length=0.05
# )

ps.show()


